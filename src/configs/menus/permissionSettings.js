/* export default [
   { icon: 'mdi-email-outline', key: 'menu.email', text: 'Email', link: '/apps/email' },
   { icon: 'mdi-forum-outline', key: 'menu.chat', text: 'Chat', link: '/apps/chat' },
   { icon: 'mdi-format-list-checkbox', key: 'menu.todo', text: 'Todo', link: '/apps/todo' },
   { icon: 'mdi-view-column-outline', key: 'menu.board', text: 'Kanban Board', link: '/apps/board' },
  { icon: 'mdi-home-modern', key: 'Masters', text: 'Masters', regex: /^\/ecommerce/,
    items: [
       { key: 'PrintReceipt', text: 'PrintReceipt', link: '/ecommerce/list' },
      { key: 'Stock Items', text: 'StockItems', link: '/ecommerce/list' },
      { key: 'Customer', text: 'Customer', link: '/ecommerce/product-details' },
      { key: 'Supplier', text: 'Supplier', link: '/ecommerce/orders' },
      { key: 'Ledger', text: 'Ledger', link: '/ecommerce/cart' },
      { key: 'Measure Unit', text: 'MeasureUnit', link: '/ecommerce/orders' },
      { key: 'Godown', text: 'Godown', link: '/ecommerce/cart' },
      { key: 'Category', text: 'Category', link: '/ecommerce/cart' }
    ]
  },
  { icon: 'mdi-briefcase', key: 'Vouchers', text: 'Vouchers', regex: /^\/users/,
    items: [
      { key: 'Order Form', text: 'Order', link: '/users/list' },
       { key: 'Receipt', text: 'Receipt', link: '/users/edit' },
      { key: 'Payment', text: 'Payment', link: '/users/list' },
      { key: 'Contra', text: 'Contra', link: '/users/edit' },
      { key: 'Delivery Note', text: 'DeliveryNote', link: '/users/list' },
      { key: 'Sales', text: 'Sales', link: '/users/edit' },
      { key: 'Sales Return', text: 'SalesReturn', link: '/users/list' },
      { key: 'Quotation', text: 'Quotation', link: '/users/edit' },
      { key: 'Purchase', text: 'Purchase', link: '/users/list' },
      { key: 'Purchase Return', text: 'PurchaseReturn', link: '/users/edit' },
      { key: 'Goods Receipt', text: 'GoodsReceipt', link: '/users/list' },
      { key: 'Journal', text: 'Journal', link: '/users/edit' }
    ]
  },
  { icon: 'mdi-book-multiple', key: 'Registers', text: 'Registers', regex: /^\/users/,
    items: [
      { key: 'Day Book', text: 'DayBook', link: '/users/list' },
      { key: 'Order', text: 'OrderRegister', link: '/users/edit' },
      { key: 'Receipt', text: 'ReceiptRegister', link: '/users/list' },
      { key: 'Payment', text: 'PaymentRegister', link: '/users/edit' },
      { key: 'Sales', text: 'SalesRegister', link: '/users/list' },
      { key: 'Sales Return', text: 'SalesReturnRegister', link: '/users/edit' },
      { key: 'Delivery Note', text: 'DeliveryNoteRegister', link: '/users/list' },
      { key: 'Quotation', text: 'QuotationRegister', link: '/users/edit' },
      { key: 'Purchase', text: 'PurchaseRegister', link: '/users/list' },
      { key: 'Purchase Return', text: 'PurchaseReturnRegister', link: '/users/edit' },
      { key: 'Goods Receipt', text: 'GoodsReceiptRegister', link: '/users/list' },
      { key: 'Journal', text: 'JournalRegister', link: '/users/edit' }
    ]
  },
  { icon: 'mdi-details', key: 'Reports', text: 'Reports', regex: /^\/users/,
    items: [
      { key: 'BillReceivable', text: 'BillReceivable', link: '/users/list' }

    ]
  }

]
 */





import axios from 'axios';


var MenuItems = [];
var MenuItems1 = {}
var MenuItems2 = {}
var MenuItems3 = {}
var MenuItems4 = {}
var MenuItems5 = {}
var MenuItems6 = {}
var MenuItems7 = {}
var IsReturn = false
  var bURL = localStorage.getItem("CompanyURL")+"/api/";
  var userrole = localStorage.getItem('UserType');
      axios.get(bURL + "common/GetAvailableForms/" + userrole)
        .then((response)  =>  {
          localStorage.setItem('AvailableForms',response.data)
          IsReturn = true
          var PermissionObject = response.data

            MenuItems1 = { icon: 'mdi-home-modern', key: 'menu.masters', text: 'Masters', regex: "",
                          items: []
                        }
            MenuItems2 = { icon: 'mdi-briefcase', key: 'menu.Vouchers', text: 'Vouchers', regex: "",
                          items: []
                        }
            MenuItems7 = { icon: 'mdi-face-agent', key: 'menu.Service', text: 'Service', regex: "",
                        items: []
                      }
                      //  MenuItems7 = { icon: 'mdi-lifebuoy', key: 'menu.Service', text: 'Service', regex: "",
                      //   items: []
                      // }
            MenuItems3 = { icon: 'mdi-book-multiple', key: 'menu.Registers', text: 'Registers', regex: "",
                        items: []
                      }
            MenuItems4 = { icon: 'mdi-details', key: 'menu.Reports', text: 'Reports', regex: "",
                      items: []
                    }
            MenuItems5 = { icon: 'mdi-tools', key: 'menu.Tools', text: 'Tools', regex: "",
                      items: []
                    }
            MenuItems6 = { icon: 'mdi-cog', key: 'menu.Settings', text: 'Settings', regex: "",
                    items: []
                  }
            for(var i=0;i<PermissionObject.length;i++)
            {
              switch (PermissionObject[i]) {
                case "frmCreateUser":
                    MenuItems1.items.push({ key: 'menu.UserManagement', text: 'UserManagement', link: '/ecommerce/product-details' })
                    MenuItems6.items.push({ key: 'menu.Role', text: 'RoleSettings', link: '/users/list' },)
                      break;
                case "frmViewCustomers":
                    MenuItems1.items.push({ key: 'menu.Customer', text: 'Customer', link: '/ecommerce/product-details' })
                      break;
                case "frmViewSuppliers":
                        MenuItems1.items.push({ key: 'menu.Supplier', text: 'Supplier', link: '/ecommerce/product-details' })
                          break;
                case "frmViewItems":
                        MenuItems1.items.push({ key: 'menu.StockItems', text: 'StockItems', link: '/ecommerce/list' })
                          break;
                case "frmGodown":
                        MenuItems1.items.push({ key: 'menu.Godown', text: 'Godown', link: '/ecommerce/list' })
                              break;
                case "frmLedger":
                        MenuItems1.items.push({ key: 'menu.Ledger', text: 'Ledger', link: '/ecommerce/list' })
                          break;
                case "frmPurchaseperson":
                      MenuItems1.items.push({ key: 'menu.Purchaseperson', text: 'Purchaseperson', link: '/ecommerce/list' })
                        break;
                case "frmSalesperson":
                      MenuItems1.items.push({ key: 'dashboard.Salesperson', text: 'Salesperson', link: '/ecommerce/list' })
                        break;
                case "frmVehicle":
                    MenuItems1.items.push({ key: 'menu.Vehicle', text: 'Vehicle', link: '/ecommerce/list' })
                      break;
                case "frmCategory":
                    MenuItems1.items.push({ key: 'dashboard.Category', text: 'Category', link: '/ecommerce/list' })
                       break;
                case "frmRoute":
                    MenuItems1.items.push({ key: 'menu.Route', text: 'Route', link: '/ecommerce/list' })
                      break;
                case "frmProject":
                    MenuItems1.items.push({ key: 'menu.Project', text: 'Project', link: '/ecommerce/list' })
                      break;
                case "frmBank":
                  MenuItems1.items.push({ key: 'menu.Bank', text: 'Bank', link: '/ecommerce/list' })
                  break;      
                case "frmKitchen":
                  MenuItems1.items.push({ key: 'menu.Kitchen', text: 'Kitchen', link: '/users/list' })
                  break;
                case "frmKitchenMessage":
                  MenuItems1.items.push({ key: 'menu.KitchenMessage', text: 'KitchenMessage', link: '/users/list' })
                  break;
                case "frmDiningArea":
                  MenuItems1.items.push({ key: 'menu.DiningArea', text: 'DiningArea', link: '/users/list' })
                  break;
                case "frmDiningTable":
                  MenuItems1.items.push({ key: 'menu.DiningTable', text: 'DiningTable', link: '/users/list' })
                  break;
                case "frmSetTenderingLedgers":
                    MenuItems1.items.push({ key: 'menu.SetTenderingLedgers', text: 'TenderingLedgers', link: '/users/list' })
                    break;
                case "frmCostCenter":
                      MenuItems1.items.push({ key: 'menu.CostCenter', text: 'CostCenter', link: '/users/list' })
                      break;      
                case "frmOrderForm":
                    MenuItems2.items.push({ key: 'menu.SalesOrder', text: 'Order', link: '/users/list' })
                    break;
                case "frmQuotation":
                    MenuItems2.items.push({ key: 'menu.Quotation', text: 'Quotation', link: '/users/list' })
                    break;
                case  "frmSales":
                  MenuItems2.items.push({ key: 'dashboard.sales', text: 'Sales', link: '/users/list' })
                    break;
                case  "frmSalesReturn":
                      MenuItems2.items.push({ key: 'dashboard.SalesReturn', text: 'SalesReturn', link: '/users/list' })
                        break;
                case  "frmStockTransfer":
                  if(userrole !=="Administrator")
                  {
                    MenuItems2.items.push({
                      key: 'menu.StockTransfer', text: 'StockTransfer', link: '/users/edit',
                       items:[
                          { key: 'menu.StockTransfer', text: 'StockTransfer', link: '/users/list' },
                         /*  { key: 'menu.BulkDamageTransfer', text: 'BulkDamageTransfer', link: '/users/list' }, */
                          { key: 'menu.BulkStockTransfer', text: 'BulkStockTransfer', link: '/users/list' }
                          ]
                       })
                  }
                  else{
                    MenuItems2.items.push({
                      key: 'menu.StockTransfer', text: 'StockTransfer', link: '/users/edit',
                       items:[
                          { key: 'menu.StockTransfer', text: 'StockTransfer', link: '/users/list' },
                          { key: 'menu.BulkStockTransfer', text: 'BulkStockTransfer', link: '/users/list' },
                          { key: 'menu.ManageAllocation', text: 'ManageAllocation', link: '/users/list' },
                          { key: 'menu.ConfirmAllocation', text: 'ConfirmAllocation', link: '/users/list' },
                          /* { key: 'menu.BulkDamageTransfer', text: 'BulkDamageTransfer', link: '/users/list' }, */

                          ]
                       })
                  }
                    break;
               case  "frmPhysicalStock":
                    MenuItems2.items.push({ key: 'menu.PhysicalStock', text: 'PhysicalStock', link: '/users/edit' })
                    MenuItems3.items.push({ key: 'menu.PhysicalStock', text: 'PhysicalStockRegister', link: '/users/edit' })
                    break;
               case  "frmReceipt":
                  MenuItems2.items.push({ key: 'dashboard.Receipt', text: 'Receipt', link: '/users/list' })
                    break;
               case  "frmPayment":
                  MenuItems2.items.push({ key: 'menu.Payment', text: 'Payment', link: '/users/list' })
                    break;
               case  "frmContra":
                      MenuItems2.items.push({ key: 'menu.Contra', text: 'Contra', link: '/users/list' })
                        break;
               case  "frmDeliveryNote":
                      MenuItems2.items.push({ key: 'menu.DeliveryNote', text: 'DeliveryNote', link: '/users/list' })
                      MenuItems3.items.push({ key: 'menu.DeliveryNote', text: 'DeliveryNoteRegister', link: '/users/edit' },)
                      break;
                case  "frmPurchase":
                        MenuItems2.items.push({ key: 'menu.Purchase', text: 'Purchase', link: '/users/list' })
                          break;
                case  "frmPurchReturn":
                        MenuItems2.items.push({ key: 'menu.PurchaseReturn', text: 'PurchaseReturn', link: '/users/list' })
                              break;
                case  "frmJournal":
                        MenuItems2.items.push({ key: 'menu.Journal', text: 'Journal', link: '/users/list' })
                        break;
                case  "frmProjectMaterialIssue":
                        MenuItems2.items.push({ key: 'menu.ProjectMaterialIssue', text: 'ProjectMaterialIssue', link: '/users/list' })
                        break;
                case  "frmLead":
                        MenuItems2.items.push({ key: 'menu.Lead', text: 'Lead', link: '/users/list' })
                        MenuItems3.items.push({ key: 'menu.Lead', text: 'LeadRegister', link: '/users/list' })
                        break;
                case  "frmLPO":
                        MenuItems2.items.push({ key: 'menu.PurchaseOrder', text: 'PurchaseOrder', link: '/users/list' })
                        break;
                case  "frmDebitNote":
                        MenuItems2.items.push({ key: 'menu.DebitNote', text: 'DebitNote', link: '/users/list' })
                        MenuItems3.items.push({ key: 'menu.DebitNote', text: 'DebitNoteRegister', link: '/users/list' })
                        break;
                case  "frmCreditNote":
                        MenuItems2.items.push({ key: 'menu.CreditNote', text: 'CreditNote', link: '/users/list' })
                        MenuItems3.items.push({ key: 'menu.CreditNote', text: 'CreditNoteRegister', link: '/users/list' })
                      break;
                /* case "frmReceipt":
                  $('#receipt').css('display','block');
                  break;
                case "frmPayment":
                 $('#payment').css('display','block');
                  break;

                case "frmViewCustomers":
                  $('#customerreg').css('display','block');
                  break;
                case  "rptBillsReceivable":
                  $('#billsrecievable').css('display','block');
                  break;
                case  "frmContra":
                  $('#contra').css('display','block');
                  break;
                case  "frmDeliveryNote":
                  $('#deliveryNote').css('display','block');
                  $('#deliveryNotereg').css('display','block');
                  break;
                case  "frmSalesReturn":
                  $('#salesReturn').css('display','block');
                  break;
                case  "frmQuotation":
                  $('#quote').css('display','block');
                  break;
                case  "frmPurchase":
                  $('#purchase').css('display','block');
                  break;
                case  "frmGoodsReceipt":
                  $('#goodsReceipt').css('display','block');
                  break;
                case  "frmJournal":
                  $('#journal').css('display','block');
                  break;
                case  "frmSupplier":
                  $('#supplierreg').css('display','block');
                  break;
                case  "frmLedger":
                  $('#ledgerreg').css('display','block');
                  break;
                case  "frmUnit":
                  $('#measureUnit').css('display','block');
                  break;
                  case  "frmGodown":
                  $('#godown').css('display','block');
                  break;
                  case  "frmCategory":
                  $('#category').css('display','block');
                  break;
                  case  "frmItem":
                  $('#itemReg').css('display','block');
                  break;
                case  "frmPurchaseReturn":
                    items[2].items.push({ key: 'Purchase Return', text: 'PurchaseReturnRegister', link: '/users/edit' })
                    break;  */

                  case "frmPendingOrderList":
                    MenuItems3.items.push({ key: 'menu.SalesOrder', text: 'OrderRegister', link: '/users/list' })
                    break;
                  case  "frmSaleReg":
                    MenuItems3.items.push({ key: 'dashboard.sales', text: 'SalesRegister', link: '/users/list' })
                    break;
                  case  "frmSalesReturnReg":
                    MenuItems3.items.push({ key: 'dashboard.SalesReturn', text: 'SalesReturnRegister', link: '/users/edit' })
                    break;
                  case  "frmQuotationReg":
                    MenuItems3.items.push({ key: 'menu.Quotation', text: 'QuotationRegister', link: '/users/edit' })
                    break;
                  case  "frmGoodsReceiptReg":
                    MenuItems3.items.push({ key: 'dashboard.Receipt', text: 'ReceiptRegister', link: '/users/list' })
                    break;
                  case  "frmPurchReg":
                    MenuItems3.items.push({ key: 'menu.Purchase', text: 'PurchaseRegister', link: '/users/list' })
                    break;
                  case  "frmDayBook":
                    MenuItems3.items.push({ key: 'menu.DayBook', text: 'DayBook', link: '/users/list' })
                    break;
                  case  "frmJournalReg":
                    MenuItems3.items.push({ key: 'menu.Journal', text: 'JournalRegister', link: '/users/edit' })
                    break;
                  case  "frmPurchReturnReg":
                    MenuItems3.items.push({ key: 'menu.PurchaseReturn', text: 'PurchaseReturnRegister', link: '/users/edit' })
                    break;
                  case "frmReceiptReg":
                    MenuItems3.items.push({ key: 'dashboard.Receipt', text: 'ReceiptRegister', link: '/users/edit' },)
                    break;
                  case "frmPaymentReg":
                    MenuItems3.items.push({ key : 'menu.Payment', text: 'PaymentRegister', link: '/users/edit' })
                    break;
                  case  "frmProjectMaterialIssue":
                      MenuItems2.items.push({ key: 'menu.ProjectMaterialIssue', text: 'ProjectMaterialIssueRegister', link: '/users/list' })
                      break;
                  /* case  "frmLead":
                        MenuItems2.items.push({ key: 'menu.Lead', text: 'LeadRegister', link: '/users/list' })
                        break;    */
                  case  "rptDeliveryNote":
                    MenuItems3.items.push({ key: 'menu.DeliveryNote', text: 'DeliveryNoteRegister', link: '/users/edit' },)
                    break;
                  case  "rptStockTransfer":
                      MenuItems3.items.push({ key: 'menu.StockTransfer', text: 'StockTransferRegister', link: '/users/edit' },)
                      break;
                  case  "frmLPO":
                      MenuItems2.items.push({ key: 'menu.PurchaseOrder', text: 'LPORegister', link: '/users/list' })
                        break;
                  case  "frmDebitNoteReg":
                    MenuItems3.items.push({ key: 'menu.DebitNote', text: 'DebitNoteRegister', link: '/users/list' })
                        break;
                  case  "frmCreditNoteReg":
                    MenuItems3.items.push({ key: 'menu.CreditNote', text: 'CreditNoteRegister', link: '/users/list' })
                        break;

                  case  "rptBillsReceivable":
                      MenuItems4.items.push({ key: 'menu.BillsReceivable', text: 'BillReceivable', link: '/users/list' },)
                      break;
                  case  "frmLiveLocation":
                        MenuItems5.items.push({ key: 'menu.LiveLocation', text: 'LiveLocation', link: '/users/list' },)
                        break;
                  case  "frmSettings":
                        MenuItems6.items.push({ key: 'menu.Voucher', text: 'Settings', link: '/users/list' },)
                        break;
                  case  "frmLockedVouchers":
                          MenuItems6.items.push({ key: 'menu.LockUnlockVoucher', text: 'Lock/UnlockVoucher', link: '/users/list' },)
                          break;
                  case  "frmOTP":
                          MenuItems6.items.push({ key: 'menu.PasswordGeneration', text: 'PasswordGeneration', link: '/users/list' },)
                          break; 
                  case  "frmServiceBooking":
                          MenuItems7.items.push({ key: 'menu.ServiceBooking', text: 'ServiceBooking', link: '/users/list' },)
                          MenuItems3.items.push({ key: 'menu.ServiceBooking', text: 'ServiceBookingRegister', link: '/users/list' },)
                          MenuItems7.items.push({ key: 'menu.ServiceTask', text: 'ServiceTask', link: '/users/list' },)
                          MenuItems3.items.push({ key: 'menu.ServiceTask', text: 'ServiceTaskRegister', link: '/users/list' },)
                          MenuItems7.items.push({ key: 'menu.AssignServiceTask', text: 'AssignServiceTask', link: '/users/list' },)
                           break;         
                                         

              }
            }

            if(userrole ==="Administrator")
            {
              MenuItems4.items.push( { key: 'menu.ExternalReports', text: 'ExternalReports', link: '/users/list' },)
              MenuItems6.items.push( { key: 'menu.Company', text: 'Company', link: '/users/list' })
              MenuItems6.items.push( { key: 'menu.Branch', text: 'Branch', link: '/users/list' })
              MenuItems6.items.push( { key: 'menu.SalesPromotion', text: 'SalesPromotion', link: '/users/list' })
            }
            MenuItems5.items.push({ key: 'menu.DefragmentDatabase', text: 'DefragmentDatabase', link: '/users/list' },)
            MenuItems5.items.push({ key: 'menu.CheckUpdates', text: 'CheckUpdates', link: '/users/list' },)
            MenuItems6.items.push({ key: 'menu.Preferences', text: 'Preferences', link: '/users/list' },)
            MenuItems5.items.push({ key: 'Voice Testing', text: 'voice', link: '/users/list' },)


            localStorage.setItem('MenuItem1', JSON.stringify(MenuItems1))
            localStorage.setItem('MenuItem2', JSON.stringify(MenuItems2))
            localStorage.setItem('MenuItem3', JSON.stringify(MenuItems3))
            localStorage.setItem('MenuItem4', JSON.stringify(MenuItems4))
            localStorage.setItem('MenuItem5', JSON.stringify(MenuItems5))
            localStorage.setItem('MenuItem6', JSON.stringify(MenuItems6))
            localStorage.setItem('MenuItem7', JSON.stringify(MenuItems7))

        }, (error)  =>  {
          console.log(error)
        })

  /* while(IsReturn === false)
  {
    if(IsReturn === true)
    {
      break
    }

  } */
export default [
  MenuItems1,
  MenuItems2,
  MenuItems3,
  MenuItems4
]

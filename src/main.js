import Vue from 'vue'
import App from './App.vue'
// VUEX - https://vuex.vuejs.org/
import store from './store'
// VUE-ROUTER - https://router.vuejs.org/
import router from './router'
import axios from 'axios'
// PLUGINS
import vuetify from './plugins/vuetify'
import i18n from './plugins/vue-i18n'
import './plugins/vue-google-maps'
import './plugins/vue-shortkey'
import './plugins/vue-head'
import './plugins/vue-gtag'
import './plugins/apexcharts'
import './plugins/echarts'
import './plugins/animate'
import './plugins/clipboard'
import './plugins/moment'
import 'bootstrap'
// import {BootstrapVue,IconsPlugin} from 'bootstrap-vue'

// FILTERS
import './filters/capitalize'
import './filters/lowercase'
import './filters/uppercase'
import './filters/formatCurrency'
import './filters/formatDate'

// STYLES
// Main Theme SCSS
import './assets/scss/theme.scss'

// Animation library - https://animate.style/
import 'animate.css/animate.min.css'

import { library }  from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add( fas )
Vue.component('font-awesome-icon',FontAwesomeIcon)
// Set this to false to prevent the production tip on Vue startup.
Vue.config.productionTip = false

// Vue.use(BootstrapVue)
// Vue.use(IconsPlugin)
import 'bootstrap/dist/css/bootstrap.min.css'

import './registerServiceWorker'
// import 'bootstrap/dist/css/bootsrap.css'
// import 'bootstrap-vue/dist/bootsrap-vue.css'

/*
|---------------------------------------------------------------------
| Main Vue Instance
|---------------------------------------------------------------------
|
| Render the vue application on the <div id="app"></div> in index.html
|
| https://vuejs.org/v2/guide/instance.html
|
*/

Vue.prototype.$http = axios
export default new Vue({
  i18n,
  vuetify,
  router,
  store,
  axios,
  render: (h) => h(App)
}).$mount('#app')

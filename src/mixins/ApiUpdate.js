// src/mixins/update.js
import axios from 'axios'
export default {
  data() {
    return {
      GlobalPasswordModal:false,
      headers : {
             'Content-Type': 'application/json',
             'Authorization': 'Bearer ' + localStorage.getItem('Token')
             },
      SalesFilterItems:0,
      isReload : false,
      Settings : [],
      Loaderdialog : true,
      IsSalesOffLine: 'false'
    }
  },

  created() {
    
  },

  methods: {
    RefreshAllData(isReload)
    {
      
      this.Token = localStorage.getItem('Token')
      
      if(this.Token !== null)
      {
        
        this.Loaderdialog = true
      }
      this.Settings = JSON.parse(localStorage.getItem('Settings'))
        
             
      this.IsReferesh = true
      this.isReload = isReload
      this.SalesLedgers()
      this.GetBranchDetails()
      this.GetSettings()
      
        
        
       
       
        
       
       
        

        
        
        
        
        
        
        
       
       
        
       
    },
   
    GetVersionCode()
    {
      alert("hi");
      this.CurrentVersion = localStorage.getItem('CurrentVersion')
      this.bURL  = localStorage.getItem("CompanyURL")+"/api/"
       axios.get ( this.bURL + 'common/GetAPIversion' )
          .then((response)  =>  {
            localStorage.setItem('CurrentVersion',response.data)
          }, (error)  =>  {
            console.log(error)
          })
    },
    
    GetSalesPersons()
    {
      
       this.bURL = localStorage.getItem("CompanyURL")+"/api/";
        this.Username = localStorage.getItem('user')
      axios.get(this.bURL + 'user/GetLinkedSalesPersonsIncludingSelf/'+this.Username)
        .then((response)  =>  {
          
          localStorage.setItem('LinkedSalesPersnons', JSON.stringify(response.data))
          this.GetPendingDeliveryCustomers()
        }, (error)  =>  {
          console.log(error)
        })
    },
    GetPendingDeliveryCustomers()
    {
       this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      this.pendingsalesMan = window.localStorage.getItem('LinkedSalesPersnons')
      if (this.pendingsalesMan === null)
      {
        this.pendingsalesMan = window.localStorage.getItem('user')
      }
      else {
        this.pendingsalesMan = JSON.parse(this.pendingsalesMan)
        this.pendingsalesMan = this.pendingsalesMan.join(' , ')
      }
      
      axios.get(this.bURL + 'DeliveryNote/GetPendingDeliveryCustomers?SalesPersons=' + this.pendingsalesMan)
        .then((response)  =>  {
          
          localStorage.setItem('DeliveryPendingCustomers', JSON.stringify(response.data))
          this.GetCurrency()

        }, (error)  =>  {
          console.log(error)
        })
    },
    GetCurrency()
    {
      
      this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      axios.get(this.bURL + 'Currency')
        .then((response)  =>  {
          
          localStorage.setItem('Currency', JSON.stringify(response.data))
          this.GetBranch()

        }, (error)  =>  {
          console.log(error)
        })
    },
    GetBranch()
    {
      this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      
      axios.get(this.bURL + 'Branch')
        .then((response)  =>  {
          
          localStorage.setItem('Branches', JSON.stringify(response.data))
          this.GetCustomers()
        }, (error)  =>  {
          console.log(error)
        })
    },
    GetCustomers()
    {
      this.Salesperson = window.localStorage.getItem('user');
      this.Settings = JSON.parse(localStorage.getItem('Settings'))
      this.ActiveRouteID = localStorage.getItem("ActiveRouteId")
      this.RouteID = -1
    this.salePerson = localStorage.getItem('salePerson')
      if(this.Settings.IsRouteMandatory === 'True' )
     {
       this.UserType = localStorage.getItem('UserType')
       if(this.UserType  === "Administrator")
       {
         this.RouteID = -1
       }
       else if(this.ActiveRouteID !== null && this.ActiveRouteID !== undefined)
       {
         this.RouteID = this.ActiveRouteID
       }
       else{
       this.LinkedRoutes = JSON.parse(localStorage.getItem('LinkedRoutes'))
       this.RouteIds = []
        for(this.i=0;this.i<this.LinkedRoutes.length;this.i++)
        {
           this.RouteIds.push( this.LinkedRoutes[this.i].RouteID)
        }
        this.RouteID = this.RouteIds.join('~')
        this.RouteID = this.RouteID?this.RouteID:0
       }
     }
     
      axios.get(this.bURL + 'Customer/LoadLinkedCustomers?RouteId='+this.RouteID)
        .then((response)  =>  {
          
          localStorage.setItem('Customers', JSON.stringify(response.data))
          this.GetLinkedCustomerGroups()
        }, (error)  =>  {
          console.log(error)
        })
    },
    GetLinkedCustomerGroups()
    {
      this.bURL = localStorage.getItem("CompanyURL")+"/api/"
      axios.get(this.bURL + 'user/GetLinkedCustomerGroups/' + this.Username)
        .then((response)  =>  {
          
          localStorage.setItem('CustomerGroups', JSON.stringify(response.data))
          this.GetCustomersDetailed()
        }, (error)  =>  {
          console.log(error)
        })
    },
    GetCustomersDetailed()
    {
      this.bURL = localStorage.getItem("CompanyURL")+"/api/"
      this.Salesperson = window.localStorage.getItem('user');
       this.Settings = JSON.parse(localStorage.getItem('Settings'))
       this.ActiveRouteID = localStorage.getItem("ActiveRouteId")
       this.RouteID = -1
     this.salePerson = localStorage.getItem('salePerson')
       if(this.Settings.IsRouteMandatory === 'True' )
      {
        this.UserType = localStorage.getItem('UserType')
        if(this.UserType  === "Administrator")
        {
          this.RouteID = -1
        }
        else if(this.ActiveRouteID !== null && this.ActiveRouteID !== undefined)
        {
          this.RouteID = this.ActiveRouteID
        }
        else{
        this.LinkedRoutes = JSON.parse(localStorage.getItem('LinkedRoutes'))
        this.RouteIds = []
         for(this.i=0;this.i<this.LinkedRoutes.length;this.i++)
         {
            this.RouteIds.push( this.LinkedRoutes[this.i].RouteID)
         }
         this.RouteID = this.RouteIds.join('~')
         this.RouteID = this.RouteID?this.RouteID:0
        }
      }
      this.DateFrom =  '1899-01-01T00:00:00'
      this.DateTo = new Date().toISOString().substr(0, 10)
      axios.get(this.bURL + 'Customer/LoadLinkedCustomersDetailed?&RouteID='+this.RouteID +"&DateFrom=" + this.DateFrom + "&DateTo=" +this.DateTo)
        .then((response)  =>  {
          
          localStorage.setItem('CustomerObject', JSON.stringify(response.data))
          this.GetCustomerLocations()
        }, (error)  =>  {
          console.log(error)
        })
    },
    GetCustomerLocations()
    {
      
      axios.get(this.bURL + 'customer/GetCustomerLocations')
        .then((response)  =>  {
          
          localStorage.setItem('CustomerrLocations', JSON.stringify(response.data))
          this.GetReturnItems()

        }, (error)  =>  {
          console.log(error)
        })
    },

    GetItems()
    {
      this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      
       this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      axios.get(this.bURL + 'Item/GetItemsBy?ItemType=' + this.SalesFilterItems + '&RefreshCache=true')
        .then((response)  =>  {
          
          localStorage.setItem('Items', JSON.stringify(response.data))
          this.GetSalesPersons()

        }, (error)  =>  {
          console.log(error)
        })
    },
    GetReturnItems()
    {
      
       this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      /* axios.get(this.bURL + 'Item') */
      axios.get(this.bURL + 'Item/GetReturnItemsBy')

        .then((response)  =>  {
          
          localStorage.setItem('ReturnsItems', JSON.stringify(response.data))
          this.GetReceivedBy()

        }, (error)  =>  {
          console.log(error)
        })
    },
    GetSettings()
    {
      this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      this.UserRoll = window.localStorage.getItem("UserType")
      this.Branchcode = window.localStorage.getItem("Branchcode")
      this.Branchcode = this.Branchcode?this.Branchcode:"MB"
      
      axios.get(this.bURL + 'common/GetSettingsDictionary?BranchCode=' + this.Branchcode)
        .then((response)  =>  {
          
          this.Settings = response.data
          localStorage.setItem('Settings', JSON.stringify(response.data))
          this.SalesFilterItems = this.Settings.SalesFilterItems
          if(this.SalesFilterItems === "True")
            this.SalesFilterItems = 0
          else
            this.SalesFilterItems = 2
          this.GetItems()
        }, (error)  =>  {
          console.log(error)
        })
    },
    GetReceivedBy()
    {
      this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      this.Username = localStorage.getItem('user')
      
      axios.get(this.bURL + 'User/GetLinkedUsernames/' + this.Username + ',True,False,True')
        .then((response)  =>  {
          
          localStorage.setItem('ReceivedBy', JSON.stringify(response.data))
          this.GetCashBankLedgerNames()
        }, (error)  =>  {
          console.log(error)
        })
    },
    GetCashBankLedgerNames()
    {
      
      this.bURL = localStorage.getItem("CompanyURL")+"/api/";
       this.Username = localStorage.getItem('user')
      axios.get(this.bURL + 'User/GetLinkedCashBankLedgers/' + this.Username )
        .then((response)  =>  {
          
          localStorage.setItem('CashBankLedgerNames', JSON.stringify(response.data))
          this.GetLinkedGodowns()

        }, (error)  =>  {
          console.log(error)
        })
    },
    GetPurchasePersons()
    {
      
       this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      axios.get(this.bURL + 'PurchasePerson')
        .then((response)  =>  {
          
          localStorage.setItem('LinkedPurchasePersons', JSON.stringify(response.data))
          this.GetGodowns()
        }, (error)  =>  {
          console.log(error)
        })
    },
    GetLinkedGodowns()
    {
       this.bURL = localStorage.getItem("CompanyURL")+"/api/";
       this.salePerson = localStorage.getItem('salePerson')
       this.BranchCode = localStorage.getItem('BranchCode')
       this.DeviceDefaultBCode = localStorage.getItem('DeviceDefaultBCode')
       if (this.BranchCode !== '#' && this.BranchCode !== '')
       {
           this.IsDisableBranch = true   
           this.BranchCode = this.BranchCode
       }
       else  if (this.DeviceDefaultBCode !== null && this.DeviceDefaultBCode !== ''){
         this.BranchCode = this.DeviceDefaultBCode
       }
       else
       {
         this.BranchCode = ''
       }
       this.Settings = JSON.parse(localStorage.getItem('Settings'))
       this.ActiveRouteID = localStorage.getItem("ActiveRouteId")
       this.RouteID = -1
      if(this.Settings.IsRouteMandatory === 'True'  )
      {
        this.UserType = localStorage.getItem('UserType')
        if(this.UserType  === "Administrator")
        {
          this.RouteID = -1
          this.BranchCode = ''
        }
        else if(this.ActiveRouteID !== null && this.ActiveRouteID !== undefined)
        {
          this.RouteID = this.ActiveRouteID
        }
        else{
        this.LinkedRoutes = JSON.parse(localStorage.getItem('LinkedRoutes'))
        this.RouteIds = []
         for(this.i=0;this.i<this.LinkedRoutes.length;this.i++)
         {
            this.RouteIds.push( this.LinkedRoutes[this.i].RouteID)
         }
         this.RouteID = this.RouteIds.join('~')
         this.RouteID = this.RouteID?this.RouteID:0
        }
      }
      
         /* axios.get(this.bURL + 'godown/GetGodownsBy?BranchCode=' + BranchCode + "&ActiveStatus=Active&GodownType=Sales") */
       
        axios.get(this.bURL + 'Godown/GetLinkedGodowns?ActiveRouteID=' + this.RouteID + '&BranchCode=' + this.BranchCode)
        .then((response)  =>  {
          
         this.SalesGodowns = []
         this.DamageGodowns = []
         this.LinkedFullGodowns = []
          for(this.i = 0;this.i<response.data.length;this.i++)
          {
            this.LinkedFullGodowns.push(response.data[this.i].GodownName)

            if(response.data[this.i].GodownType !== "E" && response.data[this.i].IsSales === true){
              this.SalesGodowns.push(response.data[this.i].GodownName)
            }
            else if(response.data[this.i].GodownType === "E")
            {
                this.DamageGodowns.push(response.data[this.i].GodownName)
            }
          }
          localStorage.setItem('LinkedGodowns',JSON.stringify(this.SalesGodowns))
          localStorage.setItem('LinkedDamageGodowns',JSON.stringify(this.DamageGodowns))
          localStorage.setItem('LinkedFullGodowns',JSON.stringify(this.LinkedFullGodowns))
          this.GetPurchasePersons()

        }, (error)  =>  {
          this.error = false
          console.log(error)
        })
    },
     GetGodowns()
    {
      
       this.bURL = localStorage.getItem("CompanyURL")+"/api/";
       this.BranchCode = localStorage.getItem('BranchCode')
       this.DeviceDefaultBCode = localStorage.getItem('DeviceDefaultBCode')
       if (this.BranchCode !== '#' && this.BranchCode !== '')
       {
           this.IsDisableBranch = true
           this.BranchCode = this.BranchCode
       }
       else  if (this.DeviceDefaultBCode !== null && this.DeviceDefaultBCode !== ''){
         this.BranchCode = this.DeviceDefaultBCode
       }
       else
       {
         this.BranchCode = 'All'
       }
         axios.get(this.bURL + 'godown/GetGodownsBy?BranchCode=' + this.BranchCode + "&ActiveStatus=Active")
        .then((response)  =>  {
          
          localStorage.setItem('Godowns',JSON.stringify(response.data))
          this.GetCompany()
        }, (error)  =>  {
          this.error = false
          console.log(error)
        })
    },
    GetDamageGodowns()
    {
      
       this.bURL = localStorage.getItem("CompanyURL")+"/api/";
       this.BranchCode = localStorage.getItem('BranchCode')
       this.DeviceDefaultBCode = localStorage.getItem('DeviceDefaultBCode')
       if (this.BranchCode !== '#' && this.BranchCode !== '')
       {
          this.IsDisableBranch = true
           this.BranchCode = this.BranchCode
       }
       else  if (this.DeviceDefaultBCode !== null && this.DeviceDefaultBCode !== ''){
         this.BranchCode = this.DeviceDefaultBCode
       }
       else
       {
         this.BranchCode = 'All'
       }
         axios.get(this.bURL + 'godown/GetGodownsBy?BranchCode=' + this.BranchCode + "&ActiveStatus=Active&GodownTypes=E")
        .then((response)  =>  {
          
          localStorage.setItem('DamageGodowns',JSON.stringify(response.data))
          this.GetSuppliers()
        }, (error)  =>  {
          this.error = false
          console.log(error)
        })
    },
    GetRoutes()
    {
      
      this.Settings = JSON.parse(localStorage.getItem('Settings'))
      if(this.Settings.IsRouteMandatory === 'True')
      {
        this.bURL = localStorage.getItem("CompanyURL")+"/api/";
        this.Username = localStorage.getItem('user')
        axios.get(this.bURL + 'Route/LoadLinkedRoutes?Username='+this.Username)
               .then((response)  =>  {
                
                 this.RouteObject = response.data
                  localStorage.setItem('LinkedRoutes',JSON.stringify(this.RouteObject))
                  this.GetDamageGodowns()
                 
               }, (error)  =>  {
 
               })
      }
      else{
        this.RouteObject = []
        localStorage.setItem('LinkedRoutes',JSON.stringify(this.RouteObject))
        this.GetDamageGodowns()
      }
      
    },
    
    GetSuppliers()
    {
      this.bURL = localStorage.getItem("CompanyURL")+"/api/"
      this.Salesperson = window.localStorage.getItem('user');
       this.RouteId = window.localStorage.getItem('ActiveRouteId');
       this.RouteId = this.RouteId?this.RouteId:-1
      axios.get(this.bURL + 'Supplier')
        .then((response)  =>  {
          
          localStorage.setItem('Suppliers', JSON.stringify(response.data))
          this.SelectAllAlternateUnits()
        }, (error)  =>  {
          console.log(error)
        })
    },
    GetCompany() {
      
      this.bURL = localStorage.getItem("CompanyURL")
        axios.get (this.bURL + "/api/Company" )
       .then((response)  =>  {
        
        localStorage.setItem('Country', response.data.country)
        this.GetRoutes()
         },
         error => {
           console.log(error);
         }
       );
   },
    GetRoleSettings(isReload)
    {
      
      this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      this.UserRoll = window.localStorage.getItem("UserType")
      axios.get(this.bURL + 'common/GetRoleSettingsDictionary?RoleName='+ this.UserRoll)
        .then((response)  =>  {
          
          this.IsReferesh = false
          localStorage.setItem('RoleSettings', JSON.stringify(response.data))
          this.Loaderdialog = false
          if(this.isReload === true)
          {
            this.Loaderdialog = false
            location.reload()
          }
            

        }, (error)  =>  {
          console.log(error)
        })
    },
    SalesLedgers()
    {
         //this.SelectAllAlternateUnits ()
         this.LoadSalesLedgers();
         this.LoadLedgers();
         this.LoadLedgerUnits();
         this.GetDeleteResons();
         this.LoadAllCurrencies();
         this.LoadCashLedgers();
         this.LoadBankLedgers();
         this.LoadOtherDrLedgers();
         this.SetPermission()
    },
    LoadCashLedgers() {
      this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      this.CashAccountOptions = [];
      this.UserName = window.localStorage.getItem("user");
      axios.get(this.bURL + "User/GetLinkedCashLedgers/" + this.UserName).then(
        response => {
          
          for (this.i = 0; this.i < response.data.length; this.i++) {
            if (
              this.CashAccountOptions.includes(response.data[this.i]) === false
            ) {
              this.CashAccountOptions.push(response.data[this.i]);
            }
          }

          localStorage.setItem('CashAccountOptions',JSON.stringify(this.CashAccountOptions))
        },
       (error)  =>  {
          this.error = false
          if(error.response === undefined)
          {
            this.SnackbarMessege = "Check your internet connection,then try again!"
          }
          else if(error.response.data !=="")
          {
            this.SnackbarMessege = "Failed"
          }
           else if(error.response.status  === 401)
          {
             this.GlobalPasswordModal = true
          }
           else{
             this.SnackbarMessege = "Check your internet connection,then try again!"
           }
          this.snackbar = true
        })
    },
    LoadSalesLedgers() {
      this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      this.brcode = window.localStorage.getItem("BranchCode");
      this.role = window.localStorage.getItem("UserType");
      this.brcode = this.brcode ? this.brcode : "All";
      axios
        .get(
          this.bURL +
            "Sales/GetSalesAccountLedgers?UserRole=" +
            this.role +
            "&BranchCode=" +
            this.brcode,
            {
        headers: this.headers
       }
        )
        .then(
          response => {
            localStorage.setItem('SalesAccountOptions',JSON.stringify(response.data))
          },
          (error)  =>  {
            
          this.error = false
          if(error.response === undefined)
          {
            this.SnackbarMessege = "Check your internet connection,then try again!"
          }
          else if(error.response.data !=="")
          {
            this.SnackbarMessege = "Failed"
          }
           else if(error.response.status  === 401)
          {
             this.GlobalPasswordModal = true
          }
           else{
             this.SnackbarMessege = "Check your internet connection,then try again!"
           }
          this.snackbar = true
        })
    },
    LoadBankLedgers() {
      this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      this.UserName = window.localStorage.getItem("user");
      axios.get(this.bURL + "User/GetLinkedBankLedgers/" + this.UserName).then(
        response => {
          
          localStorage.setItem('BankAccountOptions',JSON.stringify(response.data))
          this.LoadCashLedgers();
          
        },
        (error)  =>  {
          
          this.error = false
          if(error.response === undefined)
          {
            this.SnackbarMessege = "Check your internet connection,then try again!"
          }
          else if(error.response.data !=="")
          {
            this.SnackbarMessege = "Failed"
          }
           else if(error.response.status  === 401)
          {
             this.GlobalPasswordModal = true
          }
           else{
             this.SnackbarMessege = "Check your internet connection,then try again!"
           }
          this.snackbar = true
        })
    },
    LoadOtherDrLedgers() {
      this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      this.UserName = window.localStorage.getItem("user");
      axios
        .get(
          this.bURL +
            "User/LoadLinkedTenderingLedgers?Username=" +
            this.UserName +
            "&Mode=O"
        )
        .then(
          response => {
            this.OtherDrLedgers = []
            for (this.i = 0; this.i < response.data.length; this.i++) {
              if (response.data[this.i].Mode === "O") {
                this.OtherDrLedgers.push(response.data[this.i].LedgerName);
              }
            }
            localStorage.setItem('SalesOtherDrLedgers',JSON.stringify(this.OtherDrLedgers))
            
          },
          (error)  =>  {
            
          this.error = false
          if(error.response === undefined)
          {
            this.SnackbarMessege = "Check your internet connection,then try again!"
          }
          else if(error.response.data !=="")
          {
            this.SnackbarMessege = "Failed"
          }
           else if(error.response.status  === 401)
          {
             this.GlobalPasswordModal = true
          }
           else{
             this.SnackbarMessege = "Check your internet connection,then try again!"
           }
          this.snackbar = true
        })
    },
     LoadLedgers() {
      
      this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      this.UserRol = window.localStorage.getItem("UserType");
      this.BranchCode = window.localStorage.getItem("BranchCode");
      this.BranchCode = this.BranchCode ? this.BranchCode : "All";
      axios
        .get(
          this.bURL +
            "sales/GetSalesCrLedgers?UserRole=" +
            this.UserRol +
            "&BranchCode=" +
            this.BranchCode,
       {
        headers: this.headers
       }
        )
        .then(
          response => {
            localStorage.setItem('SalesLedgerOptions',JSON.stringify(response.data))
          },
         (error)  =>  {
          
          this.error = false
          if(error.response === undefined)
          {
            this.SnackbarMessege = "Check your internet connection,then try again!"
          }
          else if(error.response.data !=="")
          {
            this.SnackbarMessege = "Failed"
          }
           else if(error.response.status  === 401)
          {
             this.GlobalPasswordModal = true
          }
           else{
             this.SnackbarMessege = "Check your internet connection,then try again!"
           }
          this.snackbar = true
        })
    },
    LoadLedgerUnits() {
      this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      axios.get(this.bURL + "item/GetItemUnits").then(
        response => {
          
          localStorage.setItem('UnitOptions',JSON.stringify(response.data))
        },
        (error)  =>  {
          
          this.error = false
          if(error.response === undefined)
          {
            this.SnackbarMessege = "Check your internet connection,then try again!"
          }
          else if(error.response.data !=="")
          {
            this.SnackbarMessege = "Failed"
          }
           else if(error.response.status  === 401)
          {
             this.GlobalPasswordModal = true
          }
           else{
             this.SnackbarMessege = "Check your internet connection,then try again!"
           }
          this.snackbar = true
        })
    },
     GetDeleteResons() {
      this.bURL = localStorage.getItem("CompanyURL") + "/api/";
      axios.get(this.bURL + "Common/GetDeleteInvoiceReasons").then(
        response => {
          
          localStorage.setItem('SalesDeleteReasonOptions',JSON.stringify(response.data))
        },
        (error)  =>  {
          
          this.error = false
          if(error.response === undefined)
          {
            this.SnackbarMessege = "Check your internet connection,then try again!"
          }
          else if(error.response.data !=="")
          {
            this.SnackbarMessege = "Failed"
          }
           else if(error.response.status  === 401)
          {
             this.GlobalPasswordModal = true
          }
           else{
             this.SnackbarMessege = "Check your internet connection,then try again!"
           }
          this.snackbar = true
        })
    },
     LoadAllCurrencies() {
      this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      axios.get(this.bURL + "Currency/LoadAllCurrencies").then(
        response => {
          localStorage.setItem('CurrencyObject',JSON.stringify(response.data))
        },
         (error)  =>  {
          
          this.error = false
          if(error.response === undefined)
          {
            this.SnackbarMessege = "Check your internet connection,then try again!"
          }
          else if(error.response.data !=="")
          {
            this.SnackbarMessege = "Failed"
          }
           else if(error.response.status  === 401)
          {
             this.GlobalPasswordModal = true
          }
           else{
             this.SnackbarMessege = "Check your internet connection,then try again!"
           }
          this.snackbar = true
        })
    },
    SetPermission() {
      this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      this.userType = window.localStorage.getItem("UserType");
      axios
        .get(
          this.bURL + "common/GetFormPermission/" + this.userType + ",frmSales"
        )
        .then(
          response => {
               localStorage.setItem('SalesPermissions',JSON.stringify(response.data))
        },
         (error)  =>  {
          
          this.error = false
          if(error.response === undefined)
          {
            this.SnackbarMessege = "Check your internet connection,then try again!"
          }
          else if(error.response.data !=="")
          {
            this.SnackbarMessege = "Failed"
          }
           else if(error.response.status  === 401)
          {
             this.GlobalPasswordModal = true
          }
           else{
             this.SnackbarMessege = "Check your internet connection,then try again!"
           }
          this.snackbar = true
        })
    },
    SelectAllAlternateUnits() {
      this.bURL = localStorage.getItem("CompanyURL")+"/api/";
      this.Settings = JSON.parse(localStorage.getItem('Settings'))
      this.IsSalesOffLine =  localStorage.getItem('IsSalesOffLine')
      if(this.IsSalesOffLine === 'true' || this.IsSalesOffLine === true)
      {
      this.userType = window.localStorage.getItem("UserType");
   this.bURL = localStorage.getItem("CompanyURL") + "/api/";
    axios
      .get(
        this.bURL + "item/SelectAllAlternateUnits/"
      )
      .then(
        response => {
             localStorage.setItem('AllAlternateUnits',JSON.stringify(response.data))
             this.LinkedGodowns = JSON.parse(localStorage.getItem("LinkedGodowns"))
             this.GodownStockList = JSON.parse(localStorage.getItem("GodownStockList"))
             this.res = this.GetsWithComma(this.LinkedGodowns)
             this.SalesOffLineList = localStorage.getItem('SalesOffLineList')
             if(this.SalesOffLineList !== null)
             {
                 this.SalesOffLineList = JSON.parse(this.SalesOffLineList)
             }
             else{
               this.SalesOffLineList = []
             }
             this.IsUploaded = true
             for(this.i = 0;this.i<this.SalesOffLineList.length;this.i++)
             {
               if(this.SalesOffLineList[this.i].IsUploaded === false)
               {
                   this.IsUploaded = false
                   break
               }
             }
             if(this.IsUploaded === true)
             {
               this.GetGodownStockList(this.res)
             }
             else{
              this.GetRoleSettings()
             }
      },
       (error)  =>  {
        this.error = false
        if(error.response === undefined)
        {
          this.SnackbarMessege = "Check your internet connection,then try again!"
        }
        else if(error.response.data !=="")
        {
          this.SnackbarMessege = "Failed"
        }
         else if(error.response.status  === 401)
        {
           this.GlobalPasswordModal = true
        }
         else{
           this.SnackbarMessege = "Check your internet connection,then try again!"
         }
        this.snackbar = true
      })
    }
    else{
      this.GetRoleSettings()
    }
  },

  GetGodownStockList(Godown) {
    this.bURL = localStorage.getItem("CompanyURL")+"/api/";
    this.Loaderdialog = true;
    this.AsOnDate = new Date().toISOString().substr(0, 10);
    axios
      .get(
        this.bURL +
          "Godown/SelectGodownStockItemwiseListMinimal?Godowns=" +
          Godown +
          "&AsOnDate=1899-01-01T00:00:00"
      )
      .then(
        response => {
          if (response.data.length > 0) {
            this.list = []
            if (response.data.length > 0) {
              localStorage.setItem('GodownStockList',JSON.stringify(response.data))
            }
            else
            {
              localStorage.setItem('GodownStockList',JSON.stringify(this.list))
            }
          }
          this.GetRoleSettings()
         
        },
        (error)  =>  {
        this.error = false
        if(error.response === undefined)
        {
          this.SnackbarMessege = "Check your internet connection,then try again!"
        }
        else if(error.response.data !=="")
        {
          this.SnackbarMessege = "Failed"
        }
         else if(error.response.status  === 401)
        {
           this.GlobalPasswordModal = true
        }
         else{
           this.SnackbarMessege = "Check your internet connection,then try again!"
         }
        this.snackbar = true
      })
  },
   GetBranchDetails() {
    
    this.bURL = localStorage.getItem("CompanyURL")+"/api/";
    this.Branch = localStorage.getItem("BranchCode")
      this.brachCode = "";
      if (this.Branch  == "All" || this.Branch  == "#" || this.Branch  == "") 
        this.brachCode = "MB";
      else 
        this.brachCode = this.Branch;

      this.bURL = localStorage.getItem("CompanyURL") + "/api/";
      axios
        .get(
          this.bURL +
            "Branch/GetBranchByBranchCode?BranchCode=" +
            this.brachCode
        )
        .then(
          response => {
            localStorage.setItem('BranchObject',JSON.stringify(response.data))
          },
          (error)  =>  {
          this.error = false
          if(error.response === undefined)
          {
            this.SnackbarMessege = "Check your internet connection,then try again!"
          }
          else if(error.response.data !=="")
          {
            this.SnackbarMessege = "Failed"
          }
           else if(error.response.status  === 401)
          {
             this.GlobalPasswordModal = true
          }
           else{
             this.SnackbarMessege = "Check your internet connection,then try again!"
           }
          this.snackbar = true
        })
    },
    GetsWithComma(values) {
      values = values.join(",");

      return values;
    },

    
    
  },
}

// src/mixins/update.js
import axios from 'axios'
export default {
  data() {
    return {
      GlobalPasswordModal:false,
      headers : {
             'Content-Type': 'application/json',
             'Authorization': 'Bearer ' + localStorage.getItem('Token')
             },
      SalesFilterItems:0,
      isReload : false,
      Settings : [],
      Loaderdialog : true,
      AttachmentModal:false,
      DeleteConfirmation_Attachment:false,
      Attachments : [],
       AttachmentEditId:-1,
      AttachmentId : -1,
      AttachmentRemarks:'',
      AttachmentCaption:'',
    }
  },

  created() {
    
  },

  methods: {
    convert() {
        var file = document.querySelector("input[type=file]").files[0];
        var reader = new FileReader();
        var vm = this;
        reader.onload = function(e) {
          this.Cimg = e.target.result.replace("data:image/png;base64,", "");
  
          this.AttachmentFile = e.target.result;
  
          if(e.target.result.includes("data:image/png;base64,") )
          {
              vm.imageURL = e.target.result;
              vm.AttachmentFile = this.AttachmentFile.replace("data:image/png;base64,", "");
              //localStorage.setItem('AttachmentFile',this.AttachmentFile);
              vm.AttachmentType = ".png"
              //localStorage.setItem('AttachmentType' , ".png")
          }
          else if(e.target.result.includes("data:image/jpeg;base64") )
          {
              vm.imageURL = e.target.result;
              vm.AttachmentFile = this.AttachmentFile.replace("data:image/jpeg;base64,", "");
              //localStorage.setItem('AttachmentFile',this.AttachmentFile);
              vm.AttachmentType = ".jpeg"
              //localStorage.setItem('AttachmentType' , ".png")
          }
          else if(e.target.result.includes("data:application/pdf;base64,"))
          {
              vm.imageURL = "/images/dash/pdf-icon.png";
              vm.AttachmentFile = this.AttachmentFile.replace("data:application/pdf;base64,", "");
              //localStorage.setItem('AttachmentFile',this.AttachmentFile);
               vm.AttachmentType =  ".pdf"
              //localStorage.setItem('AttachmentType' , ".pdf")
          }
          else if(e.target.result.includes("data:video/mp4;base64,"))
          {
              vm.imageURL = "/images/dash/video-file-icon.png";
              vm.AttachmentFile = this.AttachmentFile.replace("data:video/mp4;base64,","");
              //localStorage.setItem('AttachmentFile',this.AttachmentFile);
              vm.AttachmentType =  ".mp4"
              //localStorage.setItem('AttachmentType', ".mp4")
          }
           else if(e.target.result.includes("data:audio/ogg;base64,"))
          {
              vm.imageURL = "/images/dash/audio-file.png";
              vm.AttachmentFile = this.AttachmentFile.replace("data:audio/ogg;base64,", "");
              //localStorage.setItem('AttachmentFile',this.AttachmentFile);
              //localStorage.setItem('AttachmentType' , ".opus")
              vm.AttachmentType =  ".opus"
          }
          else if(e.target.result.includes("data:audio/mpeg;base64,"))
          {
              vm.imageURL = "/images/dash/audio-file.png";
              vm.AttachmentFile = this.AttachmentFile.replace("data:audio/mpeg;base64,", "");
              //localStorage.setItem('AttachmentFile',this.AttachmentFile);
              //localStorage.setItem('AttachmentType' , ".opus")
              vm.AttachmentType =  ".mp3"
          }
           else if(e.target.result.includes("data:application/docx;base64,"))
          {
              vm.imageURL = "/images/dash/word.jpg";
              vm.AttachmentFile = this.AttachmentFile.replace("data:application/docx;base64,", "");
              //localStorage.setItem('AttachmentFile',this.AttachmentFile);
             // localStorage.setItem('AttachmentType' , ".docx")
              vm.AttachmentType =  ".docx"
  
          }
          else if(e.target.result.includes("data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,"))
          {
              vm.imageURL = "/images/dash/word.jpg";
              vm.AttachmentFile = this.AttachmentFile.replace("data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,", "");
              //localStorage.setItem('AttachmentFile',this.AttachmentFile);
             // localStorage.setItem('AttachmentType' , ".docx")
              vm.AttachmentType =  ".docx"
  
          }
  
           else if(e.target.result.includes("data:application/txt;base64,"))
          {
              vm.imageURL = "/images/dash/word.jpg";
              vm.AttachmentFile = this.AttachmentFile.replace("data:application/txt;base64,", "");
              localStorage.setItem('AttachmentFile',this.AttachmentFile);
              //localStorage.setItem('AttachmentType' , ".txt")
              vm.AttachmentType =  ".txt"
          }
           else if(e.target.result.includes("data:application/xlsx;base64,"))
          {
              vm.imageURL = "/images/dash/Excel-2-icon.png";
              vm.AttachmentFile = this.AttachmentFile.replace("data:application/xlsx;base64,", "");
              //localStorage.setItem('AttachmentFile',this.AttachmentFile);
              vm.AttachmentType =  ".xlsx"
          }
           else if( e.target.result.includes("data:application/xlsb;base64,"))
          {
              vm.imageURL = "/images/dash/Excel-2-icon.png";
              vm.AttachmentFile = this.AttachmentFile.replace("data:application/xlsb;base64,", "");
              //localStorage.setItem('AttachmentFile',this.AttachmentFile);
              vm.AttachmentType =  ".xlsx"
          }
           else if(e.target.result.includes("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,"))
          {
              vm.imageURL = "/images/dash/Excel-2-icon.png";
              vm.AttachmentFile = this.AttachmentFile.replace("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,", "");
  
              //localStorage.setItem('AttachmentFile',this.AttachmentFile);
              vm.AttachmentType =  ".xlsx"
          }
          else
          {
            /*  vm.AttachmentFile = this.AttachmentFile.replace("data:application/pdf;base64,", "");
              //localStorage.setItem('AttachmentFile',this.AttachmentFile);
              localStorage.setItem('AttachmentType' , "Other") */
              this.SnackbarMessege = "Not Support this format, try another"
              this.snackbar = true
          }
  
  
        };
        reader.onerror = function(error) {
          alert(error);
        };
        reader.readAsDataURL(file);
      },
        choosefile() {
        document.getElementById("my_file").click();
      },
       uploadImage: function() {
        var file = document
          .querySelector('input[type=file]')
          .files[0];
        var reader = new FileReader();
        reader.onload = function(e) {
          this.ImageModal = false
          this.AttachmentFile = e.target.result;
          if(e.target.result.includes("data:image/jpeg;base64") || e.target.result.includes("data:image/png;base64,") )
          {
              this.imageURL = e.target.result
              this.AttachmentFile = this.AttachmentFile.replace("data:image/jpeg;base64,", "");
              this.AttachmentFile = this.AttachmentFile.replace("data:image/png;base64,", "");
              localStorage.setItem('AttachmentFile',this.AttachmentFile);
              localStorage.setItem('AttachmentType' , ".png")
          }
          else if(e.target.result.includes("data:application/pdf;base64,"))
          {
              this.imageURL = "/images/dash/pdf-icon.png";
              this.AttachmentFile = this.AttachmentFile.replace("data:application/pdf;base64,", "");
              localStorage.setItem('AttachmentFile',this.AttachmentFile);
              localStorage.setItem('AttachmentType' , ".pdf")
          }
          else if(e.target.result.includes("data:video/mp4;base64,"))
          {
              this.imageURL = "/images/dash/video-file-icon.png";
              this.AttachmentFile = this.AttachmentFile.replace("data:video/mp4;base64,", "");
              localStorage.setItem('AttachmentFile',this.AttachmentFile);
              localStorage.setItem('AttachmentType' , ".mp4")
          }
          else if(e.target.result.includes("data:audio/ogg;base64,"))
          {
              this.imageURL = "/images/dash/audio-file.png";
              this.AttachmentFile = this.AttachmentFile.replace("data:audio/ogg;base64,", "");
              localStorage.setItem('AttachmentFile',this.AttachmentFile);
              localStorage.setItem('AttachmentType' , ".opus")
          }
  
           else if(e.target.result.includes("data:application/docx;base64,"))
          {
              this.imageURL = "/images/dash/word.jpg";
              this.AttachmentFile = this.AttachmentFile.replace("data:application/docx;base64,", "");
              localStorage.setItem('AttachmentFile',this.AttachmentFile);
              localStorage.setItem('AttachmentType' , ".docx")
          }
           else if(e.target.result.includes("data:application/txt;base64,"))
          {
              this.imageURL = "/images/dash/word.jpg";
              this.AttachmentFile = this.AttachmentFile.replace("data:application/txt;base64,", "");
              localStorage.setItem('AttachmentFile',this.AttachmentFile);
              localStorage.setItem('AttachmentType' , ".txt")
          }
          else if(e.target.result.includes("data:application/xlsx;base64,") || e.target.result.includes("data:application/xlsb;base64,"))
          {
              this.imageURL = "/images/dash/Excel-2-icon.png";
              this.AttachmentFile = this.AttachmentFile.replace("data:application/xlsx;base64,", "");
              this.AttachmentFile = this.AttachmentFile.replace("data:application/xlsb;base64,", "");
              localStorage.setItem('AttachmentFile',this.AttachmentFile);
              localStorage.setItem('AttachmentType' , ".mp4")
          }
          else
          {
             this.AttachmentFile = this.AttachmentFile.replace("data:application/pdf;base64,", "");
              localStorage.setItem('AttachmentFile',this.AttachmentFile);
              localStorage.setItem('AttachmentType' , "Other")
          }
  
          //document.getElementById("AttachmentFile").src = e.target.result;
        };
        reader.onerror = function(error) {
          alert(error);
        };
        reader.readAsDataURL(file);
  
      },

    OpenAttachmentModal()
    {
      this.ClearAttachment()
        this.AttachmentModal = true
          setTimeout(() => {
          }, 1000);
  
    },
    AddAttachment()
    {
      this.Obj= {}
         this.Obj.AttachmentId = -1
         this.Obj.ContentB64 = this.AttachmentFile
         this.Obj.ImageSrc = "data:image/png;base64," + this.AttachmentFile
         this.Obj.FileURL = ""
         if(this.Obj.ContentB64 === null || this.Obj.ContentB64 === undefined || this.Obj.ContentB64 === "")
         {
          this.SnackbarMessege = "Please attach details"
          this.snackbar = true
          return
         }
         this.Obj.Remarks = this.AttachmentRemarks?this.AttachmentRemarks:""
         this.Obj.Caption = this.AttachmentCaption?this.AttachmentCaption:""
         this.Obj.FileExtension = this.AttachmentType
         if( this.AttachmentType === ".jpeg" ||  this.AttachmentType === ".png" )
         {
            this.Obj.IsImage = true
         }
         else{
           this.Obj.IsImage = false
         }

         this.Attachments.push(this.Obj)
         this.SnackbarMessege = "Added"
          this.snackbar = true
         this.ClearAttachment()
         this.SetAttachementImage()
         setTimeout(() => {

        }, 1000);

    },
    EditAttachement(item,i)
    {
        this.AttachmentEditId = i
        this.AttachmentId = item.AttachmentId
        this.AttachmentRemarks = item.Remarks
        this.AttachmentCaption = item.Caption
        localStorage.setItem('AttachmentFile',item.ContentB64)
        localStorage.setItem('AttachmentType',item.FileExtension)
        if(item.FileExtension === ".png" || item.FileExtension === ".jpeg")
            this.imageURL = item.ImageSrc
        else if(item.FileExtension === ".pdf")
            this.imageURL = "/images/dash/pdf-icon.png"
        else if(item.FileExtension === ".mp4")
            this.imageURL = "/images/dash/video-file-icon.png"
        else if(item.FileExtension === ".mp3")
            this.imageURL = "/images/dash/audio-file.png"
         else if(item.FileExtension === ".opus")
            this.imageURL = "/images/dash/audio-file.png"
        else if(item.FileExtension === ".xlsx" || item.FileExtension === ".xlsb")
            this.imageURL = "/images/dash/Excel-2-icon.png"
        else if(item.FileExtension === ".docx")
            this.imageURL = "/images/dash/word.jpg";
        else if(item.FileExtension === ".txt")
            this.imageURL = "/images/dash/word.jpg";

        this.AttachmentModal = true
    },
    UpdateAttachement()
    {
        this.Attachments[this.AttachmentEditId].ContentB64 = this.AttachmentFile
         this.Attachments[this.AttachmentEditId].ImageSrc = "data:image/png;base64," + this.AttachmentFile
         this.Obj.FileURL = ""
         if(this.Attachments[this.AttachmentEditId].ContentB64 === null || this.Attachments[this.AttachmentEditId].ContentB64 === "" || this.Attachments[this.AttachmentEditId].ContentB64 === undefined)
         {
          this.SnackbarMessege = "Please attach details"
          this.snackbar = true
          return
         }
         this.Attachments[this.AttachmentEditId].Remarks = this.AttachmentRemarks?this.AttachmentRemarks:""
         this.Attachments[this.AttachmentEditId].Caption = this.AttachmentCaption?this.AttachmentCaption:""
         this.Attachments[this.AttachmentEditId].FileExtension = this.AttachmentType
          if( this.AttachmentType === ".jpeg" ||  this.AttachmentType === ".png" )
         {
            this.Attachments[this.AttachmentEditId].IsImage = true
         }
         else{
           this.Attachments[this.AttachmentEditId].IsImage = false
         }
         this.AttachmentEditId = -1
          this.ClearAttachment()
         this.SetAttachementImage()
    },
     DeleteAttachment()
    {
      if(this.AttachmentEditId > -1)
       {
          this.DeleteConfirmation_Attachment = true
       }
    },
     ExecuteDeleteAttachment()
    {
          this.Attachments.splice(this.AttachmentEditId,1)
          this.ClearAttachment()
          this.DeleteConfirmation_Attachment = false

    },
    ClearAttachment()
    {
      this.AttachmentFile = ""
      this.AttachmentRemarks = ""
      this.AttachmentCaption = ""
      this.AttachmentEditId = -1,
      this.AttachmentId = -1
      this.imageURL =  'https://media.istockphoto.com/vectors/click-hand-icon-with-editable-stroke-vector-id1285064521?k=20&m=1285064521&s=612x612&w=0&h=3T0cxM3_RKVhpdVRHhxIjRIs691lY5n1eLxLglEqcmw='
    },
    DownloadFile()
    {
            this.IsNativeApp = localStorage.getItem('IsNativeApp')
            if(this.IsNativeApp === "true" || this.IsNativeApp === true)
            {
                window.interface.showPDF(this.Attachments[this.AttachmentEditId].FileURL,"Bluetooth")
            }
            else
            {
               window.open(this.Attachments[this.AttachmentEditId].FileURL, '_blank')
            }
    },


    GoBackPrevious()
    {
          this.PreviousRoute = localStorage.getItem('PreviousRoute')
          this.$router.push(this.PreviousRoute)
    }
       
  },
}

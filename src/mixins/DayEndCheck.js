// src/mixins/update.js
import axios from 'axios'
export default {
  data() {
    return {
      GlobalPasswordModal:false,
      headers : {
             'Content-Type': 'application/json',
             'Authorization': 'Bearer ' + localStorage.getItem('Token')
             },
       
    }
  },

  created() {
    
  },

  methods: {
      CheckActivRote()
      {
        this.Settings = JSON.parse(localStorage.getItem('Settings'))
        this.Saleperson = localStorage.getItem('salePerson')
        this.bURL = localStorage.getItem("CompanyURL")+"/api/";
        this.IsDayStartPermission = localStorage.getItem('IsDayStartPermission')
        if(this.Saleperson != ""  && this.Settings.IsRouteMandatory === 'True' && (this.IsDayStartPermission === 'true' || this.IsDayStartPermission === true ))
        {
            axios.get(this.bURL + 'user/GetActiveRouteID?SalePerson='+ this.Saleperson)
                .then((response)  =>  {
            if(parseFloat(response.data) > 0)
            {
               this.flag = false
            }
            else{
              localStorage.removeItem('DayStatus')
              localStorage.removeItem('ActiveRouteId')
              this.overlay = true
            }
            

        }, (error)  =>  {
          console.log(error)
        })
        }
        else
        {
            this.flag = false
        }
      },
      ExecuteUploadAllSales(Sales,IsLoading)
      {
        if(IsLoading === true)
        {
            this.Loaderdialog = true;
            this.isSignInDisabled = true;
        }
          
          this.bURL = localStorage.getItem("CompanyURL")+"/api/";
                  this.UploadList = []
                  for(this.i=0;this.i<Sales.length;this.i++)
                   {
                     if(Sales[this.i].IsUploaded === false)
                     {

                      this.SalesDrLedgers = Sales[this.i].SalesDrLedgers
                      for (this.j = 0; this.j < this.SalesDrLedgers.length; this.j++) {
                          if (this.SalesDrLedgers[this.j].Mode === "Cash") {
                            this.SalesDrLedgers[this.j].Mode = "C";
                          }
                          if (this.SalesDrLedgers[this.j].Mode === "Bank") {
                              this.SalesDrLedgers[this.j].Mode = "B";
                          }
                          if (this.SalesDrLedgers[this.j].Mode === "Other") {
                              this.SalesDrLedgers[this.j].Mode = "O";
                          }
                          if (this.SalesDrLedgers[this.j].Mode === "Credit") {
                            this.SalesDrLedgers[this.j].Mode = "D";
                          }
                          if (this.SalesDrLedgers[this.j].Amount === 0) {
                            this.SalesDrLedgers.splice(this.j, 1);
                            this.j--;
                          }
                      }
                      Sales[this.i].SalesDrLedgers = this.SalesDrLedgers;
                         this.UploadList.push(Sales[this.i])
                     }
                   }
            axios
              .post(
                this.bURL +
                  "Sales/SalesPostMukltiple",
                this.UploadList,
                {
                  headers: this.headers
                }
              )
              .then(
                response => {
                  this.Loaderdialog = false;
                  this.isSignInDisabled = false;
                  this.UploadAllSaleModal = false
    
                    for(this.i=0;this.i<Sales.length;this.i++)
                    {
                      for(this.j=0;this.j<response.data.length;this.j++)
                      {
                        if(Sales[this.i].IsUploaded === false)
                        {
                            this.Result = response.data[this.j].split('~')
                            if(this.Result[1] === "GUID already exist" || this.Result[1] === "Success")
                            {
                              if(this.Result[0] === Sales[this.i].ReferNo)
                              {
                                  Sales[this.i].IsUploaded = true
                              }
                            }
                            else
                            {
                              if(this.Result[0] === Sales[this.i].ReferNo)
                              {
                                Sales[this.i].Reason =  this.Result[1]  
                              } 
                            }
                        }
                      }
                    }
                   localStorage.setItem('SalesOffLineList',JSON.stringify(Sales))
                   if(IsLoading === true)
                   {
                      location.reload()
                   }
                  
    
                  //setTimeout(  location.reload() , 5000)
                },
                 (error)  =>  {
                    this.Loaderdialog = false;
                  this.isSignInDisabled = false;
                if(IsLoading === true)
                {
                  if (error.response === undefined) {
                    this.SnackbarMessege = "Check your internet connection,then try again!"
                  } else if (error.response.data !== "") {
                    this.SnackbarMessege = error.response.data;
                  }
                    else if(error.response.status  === 401)
                  {
                    this.GlobalPasswordModal = true
                  }
                  else {
                    this.SnackbarMessege = "Check your internet connection,then try again!"
                  }
                  this.snackbar = true;
                }
              })
    
      },
      currencyToWords(CurrencySymbol,num=0, curr, format={}) {
        this.SubUnit  = ""
        this.fraction  = 0
        this.CurrencyObj = JSON.parse(localStorage.getItem('CurrencyObject'))
        for(this.i=0;this.i<this.CurrencyObj.length;this.i++)
        {
           if(this.CurrencyObj[this.i].CurrencySymbol===CurrencySymbol)
           {
                this.SubUnit  = this.CurrencyObj[this.i].DecimalSymbol
                this.fraction  = this.CurrencyObj[this.i].DecimalPlaces
           }
        }
        curr = {
          //country    : "Omani",  // country Name
          //majorSingle: "Riyal", // Major Unit Single
          //majorPlural: "Riyals",// Major Unit Plural
          country    : "",  // country Name blank for avoid Final result
          majorSingle: "", // Major blank for avoid Final result
          majorPlural: "",// Major blank for avoid Final result
          minorSingle: this.SubUnit,  // Minor Sub-Unit Single
          minorPlural: this.SubUnit,  // Minor Sub-Unit Plural
          fraction   : this.fraction,        // Decimal Places
          };

        format=format.minor;
        format ??= "";
        num=(num+="").split((0.1).toLocaleString().substring(1,2));
        let frc = (num[1]+"000").substring(0,curr.fraction), a="and ",
            cc  = " "+curr.country+(curr.country?" ":"")+(num[0]>1?curr.majorPlural:curr.majorSingle),
            out = numToWords(num[0])+(format=="fraction" && num[1]?"":cc);
        if (num[1] && curr.fraction) {
            let sub=frc>1?curr.minorPlural:curr.minorSingle;
            if      (format=="numeric")  out+=a+(+frc)+" "+sub;
            else if (format=="fraction") out+=a+(+frc)+"/1"+"0".repeat(curr.fraction)+cc;
            else    out+=a+numToWords(frc)+" "+sub;
        }
        out += " Only"
        return out;
        //----------------------------------------------------------------
        function numToWords(num = 0) {
        if (num == 0) return "Zero";
        num= ("0".repeat(2*(num+="").length%3)+num).match(/.{3}/g);
        let out="",
            T10s=["","One","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Eleven","Twelve","Thirteen","Fourteen","Fifteen","Sixteen","Seventeen","Eighteen","Nineteen"],
            T20s=["","","Twenty","Thirty","Forty","Fifty","Sixty","Seventy","Eighty","Ninety"],
            sclT=["","Thousand","Million","Billion","Trillion","Quadrillion"];
        return num.forEach((n,i) => {
        if (+n) {
         let h=+n[0], t=+n.substring(1), scl=sclT[num.length-i-1];
         out+=(out?" ":"")+(h?T10s[h]+" Hundred":"")+(h && t?" ":"")+(t<20?T10s[t]:T20s[+n[1]]+(+n[2]?" ":"")+T10s[+n[2]]);
         out+=(out && scl?" ":"")+scl;
        }}),out;
        }
        },
    GoBackPrevious()
    {
          this.PreviousRoute = localStorage.getItem('PreviousRoute')
          this.ActiveRoute =  localStorage.getItem('ActiveRoute')
          localStorage.setItem('ActiveRoute',this.ActiveRoute)
          this.$router.push(this.PreviousRoute)
    }
  },
}

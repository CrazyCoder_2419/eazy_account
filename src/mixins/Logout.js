// src/mixins/update.js
import axios from 'axios'
export default {
    data() {
      return {
        bURL:localStorage.getItem("CompanyURL")+"/api/"
      }
    },
  
    created() {
     
    },
  
    methods: {
        logout(IFrameUrl)
        {
          this.bURL = this.bURL = localStorage.getItem("CompanyURL")+"/api/";
           axios.get(this.bURL + 'user/UpdateLogOutTime?Username='+ localStorage.getItem('user') )
            .then((response)  =>  {
              this.CompanyURL = localStorage.getItem('CompanyURL')
            this.CompanyName = localStorage.getItem('Companyname')
            this.CompanyLogo = localStorage.getItem('Companylogo')
            this.GlobalTheme = localStorage.getItem('GlobalTheme')
            this.CurrentVersion = localStorage.getItem('CurrentVersion')
            this.IsSalesOffLine = localStorage.getItem('IsSalesOffLine')
            this.SalesReferNoContent = localStorage.getItem('SalesReferNoContent')
            if(this.SalesReferNoContent !== null)
            {
                this.SalesReferNoContent = JSON.parse(this.SalesReferNoContent)
            }
            else{
              this.SalesReferNoContent = {}
            }
            this.SalesOffLineList = localStorage.getItem('SalesOffLineList')
            if(this.SalesOffLineList !== null)
            {
                this.SalesOffLineList = JSON.parse(this.SalesOffLineList)
            }
            else{
              this.SalesOffLineList = []
            }

            this.GodownStockList = localStorage.getItem('GodownStockList')
            if(this.GodownStockList !== null)
            {
                this.GodownStockList = JSON.parse(this.GodownStockList)
            }
            else{
              this.GodownStockList = []
            }
    
            this.Printer = localStorage.getItem('Printer')
            this.Printer =this.Printer?this.Printer:""
            this.Papersize = localStorage.getItem('Papersize')
            this.Papersize = this.Papersize?this.Papersize:""

            this.DeviceDefaultBCode  =localStorage.getItem('DeviceDefaultBCode')
            localStorage.clear()
    
            localStorage.setItem('CompanyURL',this.CompanyURL)
            localStorage.setItem('Companyname',this.CompanyName)
            localStorage.setItem('Companylogo',this.CompanyLogo)
            localStorage.setItem('Printer',this.Printer)
            localStorage.setItem('Papersize',this.Papersize)
            localStorage.setItem('GlobalTheme',this.GlobalTheme)
            localStorage.setItem('CurrentVersion',this.CurrentVersion)
            if(this.IsSalesOffLine === null || this.IsSalesOffLine === 'null')
            {
              localStorage.setItem('IsSalesOffLine','false')
            }
            else{
              localStorage.setItem('IsSalesOffLine',this.IsSalesOffLine)
            }
            localStorage.setItem('SalesReferNoContent',JSON.stringify(this.SalesReferNoContent))
            localStorage.setItem('SalesOffLineList',JSON.stringify(this.SalesOffLineList))
            localStorage.setItem('GodownStockList',JSON.stringify(this.GodownStockList))
           
               //this.IFrameUrl = 'https://docs.google.com/viewer?url='+this.url+' &embedded=true'
               if(IFrameUrl !== undefined )
               {
                  localStorage.setItem('IFramePrintUrl',IFrameUrl)
                  localStorage.setItem('IframeLoader','true')
                  this.$router.push('/dashboard/masters/iframepdf')
               }
               else{
                 localStorage.setItem('IsReload',true)
                 this.$router.push('/auth/signin')
               }
                   
            
            }, (error)  =>  {
              console.log(error)
            })
    
        },
    },
  }
  
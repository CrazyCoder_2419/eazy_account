// src/mixins/update.js
import axios from 'axios'
export default {
  data() {
    return {
      GlobalPasswordModal:false,
      headers : {
             'Content-Type': 'application/json',
             'Authorization': 'Bearer ' + localStorage.getItem('Token')
             },
      SalesFilterItems:0,
      isReload : false,
      Settings : [],
      Loaderdialog : true,
      IsSalesOffLine: 'false'
    }
  },

  created() {
    
  },

  methods: {
 ExecuteUploadAllSales(Sales,IsLoading)
  {
    if(IsLoading === true)
    {
        this.Loaderdialog = true;
    }
      
      this.isSignInDisabled = true;
      
      this.bURL = localStorage.getItem("CompanyURL")+"/api/";
     
          this.Loaderdialog = true;
              this.isSignInDisabled = true;
              this.UploadList = []
              for(this.i=0;this.i<Sales.length;this.i++)
               {
                 if(Sales[this.i].IsUploaded === false)
                 {   
                  this.SalesDrLedgers = this.Sales[this.i].SalesDrLedgers
                  for (this.j = 0; this.j < this.SalesDrLedgers.length; this.j++) {
                      if (this.SalesDrLedgers[this.j].Mode === "Cash") {
                        this.SalesDrLedgers[this.j].Mode = "C";
                      }
                      if (this.SalesDrLedgers[this.j].Mode === "Bank") {
                          this.SalesDrLedgers[this.j].Mode = "B";
                      }
                      if (this.SalesDrLedgers[this.j].Mode === "Other") {
                          this.SalesDrLedgers[this.j].Mode = "O";
                      }
                      if (this.SalesDrLedgers[this.j].Mode === "Credit") {
                        this.SalesDrLedgers[this.j].Mode = "D";
                      }
                      if (this.SalesDrLedgers[this.j].Amount === 0) {
                        this.SalesDrLedgers.splice(this.j, 1);
                        this.j--;
                      }
                  }
                  Sales[this.i].SalesDrLedgers = this.SalesDrLedgers;
                     this.UploadList.push(Sales[this.i])
                 }
               }
        axios
          .post(
            this.bURL +
              "Sales/SalesPostMukltiple",
            this.UploadList,
            {
              headers: this.headers
            }
          )
          .then(
            response => {
              this.Loaderdialog = false;
              this.isSignInDisabled = false;
              this.UploadAllSaleModal = false

                for(this.i=0;this.i<Sales.length;this.i++)
                {
                  for(this.j=0;this.j<response.data.length;this.j++)
                  {
                    if(Sales[this.i].IsUploaded === false)
                    {
                        this.Result = response.data[this.j].split('~')
                        if(this.Result[1] === "GUID already exist" || this.Result[1] === "Success")
                        {
                          if(this.Result[0] === Sales[this.i].ReferNo)
                          {
                              Sales[this.i].IsUploaded = true
                          }
                        }
                        else
                        {
                          if(this.Result[0] === Sales[this.i].ReferNo)
                          {
                            Sales[this.i].Reason =  this.Result[1]  
                          } 
                        }
                    }
                  }
                }
               localStorage.setItem('SalesOffLineList',JSON.stringify(Sales))
               if(IsLoading === true)
               {
                  location.reload()
               }
              

              //setTimeout(  location.reload() , 5000)
            },
             (error)  =>  {
                this.Loaderdialog = false;
              this.isSignInDisabled = false;
            if(IsLoading === true)
            {
              if (error.response === undefined) {
                this.SnackbarMessege = "Check your internet connection,then try again!"
              } else if (error.response.data !== "") {
                this.SnackbarMessege = error.response.data;
              }
                else if(error.response.status  === 401)
              {
                this.GlobalPasswordModal = true
              }
              else {
                this.SnackbarMessege = "Check your internet connection,then try again!"
              }
              this.snackbar = true;
            }
          })

  },
   
       
  },
}

import Vue from 'vue'
import Router from 'vue-router'
import AppsRoutes from './apps.routes'
import UIRoutes from './ui.routes'
import PagesRoutes from './pages.routes'
import UsersRoutes from './users.routes'
import EcommerceRoutes from './ecommerce.routes'
import LandingRoutes from './landing.routes'
Vue.use(Router)
export const routes = [{
        path: '/',
        redirect: '/auth/InitialPage',
        meta: {
            hideNavbar: true,
        }
    },

    //*************************** Starts E recp ******************** */
    {
        path: '/e_recipe_order_area',
        name: 'e_recipe_order_area',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/e_recipe_views/order_e_recipe.vue')
    },
    {
        path: '/order_e_booking',
        name: 'order_e_booking',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/e_recipe_views/order_area_.vue')
    },

    //*************************** Starts E recp ******************** */
    {
        path: '/e_recipe_compo',
        name: 'e_recipe_compo',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/e_recipe_views/landing_e_recipe.vue')
    },
    {
        path: '/e_recipe_dining',
        name: 'e_recipe_dining',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/e_recipe_views/dining_area_.vue')
    },
    {
        path: '/DiningArea',
        name: 'DiningArea',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/e_recipe_views/Masters/DiningArea.vue')
    },
    {
        path: '/DiningTable',
        name: 'DiningTable',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/e_recipe_views/Masters/DiningTable.vue')
    },
    {
        path: '/Kitchens',
        name: 'Kitchens',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/e_recipe_views/Masters/Kitchens.vue')
    },
    {
        path: '/KitchenMessage',
        name: 'KitchenMessage',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/e_recipe_views/Masters/KitchenMessage.vue')
  },


    //*************************** ends E recp ******************** */
    {
        path: '/dashboard/analytics',
        name: 'dashboard-analytics',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/dashboard/DashboardPage.vue')
  },


    {
        path: '/dashboard/customerdetails',
        name: 'customerdetails',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/dashboard/Customerdetails.vue')
    },

    {
        path: '/dashboard/testing',
        name: 'testing',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/dashboard/testing.vue')
    },
    {
        path: '/dashboard/order_form',
        name: 'order-form',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/OrderForm.vue')
    },
    {
        path: '/dashboard/enroll',
        name: 'enroll',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/dashboard/enrollPage.vue')
    },
    {
        path: '/dashboard/nav',
        name: 'navigation',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/dashboard/navigationView.vue')
    },
    {
        path: '/dashboard/order_form1',
        name: 'order_form1',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/OrderFormOld.vue')
    },



    {
        path: '/dashboard/dashboard',
        name: 'dashboard1',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/dashboard/dashboard.vue')
    },


    /* ***************************** Vouchers Starts Here  ************************************** */
    {
        path: '/dashboard/vouchers/physicalstock',
        name: 'physicalstock',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/PhysicalStock.vue')
    },
    {
        path: '/dashboard/vouchers/sales',
        name: 'sales',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/Sales.vue')
    },
    {
        path: '/dashboard/vouchers/salesOffline',
        name: 'salesOffline',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/SalesOffline.vue')
    },
    {
        path: '/dashboard/vouchers/salesReturn',
        name: 'salesReturn',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/SalesReturn.vue')
    },
    {
        path: '/dashboard/vouchers/quotation',
        name: 'quotation',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/Quotation.vue')
    },
    {
        path: '/dashboard/vouchers/journal',
        name: 'journal',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/Journal.vue')
    },
    {
        path: '/dashboard/order_reg',
        name: 'order-register',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/OrderRegister.vue')
    },
    {
        path: '/dashboard/preference',
        name: 'preference',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/Preference.vue')
    },
    {
        path: '/dashboard/Notifications',
        name: 'Notifications',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/Notifications.vue')
    },
    {
        path: '/dashboard/deviceRegister',
        name: 'deviceRegister',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/DeviceRegister.vue')
    },
      {
        path: '/dashboard/settings/GeneratePassword',
        name: 'GeneratePassword',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Settings/GeneratePassword.vue')
    },
    {
        path: '/dashboard/vouchers/stockTransfer',
        name: 'stockTransfer',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/StockTransfer.vue')
    },
    {
        path: '/dashboard/vouchers/StockTransferOut',
        name: 'StockTransferOut',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/StockTransferOut.vue')
    },
    {
        path: '/dashboard/vouchers/StockTransferIn',
        name: 'StockTransferIn',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/StockTransferIn.vue')
    },
    {
        path: '/dashboard/reciept',
        name: 'recieptNew',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/dashboard/Reciept.vue')
    },
    {
        path: '/dashboard/Vouchers/reciept',
        name: 'reciept',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/Receipt.vue')
    },
    {
        path: '/dashboard/Vouchers/payment',
        name: 'payment',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/Payment.vue')
    },
    {
        path: '/dashboard/Vouchers/contra',
        name: 'contra',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/Contra.vue')
    },
    {
        path: '/dashboard/Vouchers/purchase',
        name: 'purchase',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/Purchase.vue')
    },
    {
        path: '/dashboard/Vouchers/purchaseReturn',
        name: 'purchaseReturn',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/PurchaseReturn.vue')
    },
    {
        path: '/dashboard/Vouchers/deliveryNote',
        name: 'deliveryNote',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/DeliveryNote.vue')
    },
    {
        path: '/dashboard/Vouchers/lead',
        name: 'lead',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/Lead.vue')
    },
    {
        path: '/dashboard/Vouchers/ProjectMaterial',
        name: 'ProjectMaterial',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/ProjectMaterial.vue')
    },
    {
        path: '/dashboard/Vouchers/ManageAllocation',
        name: 'ManageAllocation',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/ManageAllocation.vue')
    },
    {
        path: '/dashboard/Vouchers/ConfirmAllocation',
        name: 'ConfirmAllocation',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/ConfirmAllocation.vue')
    },
    {
        path: '/dashboard/Vouchers/Lead',
        name: 'Lead',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/Lead.vue')
    },
    {
        path: '/dashboard/Vouchers/PurchaseOrder',
        name: 'PurchaseOrder',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/PurchaseOrder.vue')
    },
    {
        path: '/dashboard/Vouchers/DebitNote',
        name: 'DebitNote',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/DebitNote.vue')
    },
    {
        path: '/dashboard/Vouchers/CreditNote',
        name: 'CreditNote',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Vouchers/CreditNote.vue')
    },
    /* ***************************** Vouchers Ends Here  ************************************** */






    /* ***************************** Register Satarts Here  ************************************** */
    {
        path: '/dashboard/payment_reg',
        name: 'paymentReg',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/PaymentRegister.vue')
    },
    {
        path: '/dashboard/receipt_reg',
        name: 'receiptReg',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/ReceiptRegister.vue')
    },
    {
        path: '/dashboard/sales_reg',
        name: 'salesReg',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/SalesRegister.vue')
    },
    {
        path: '/dashboard/sales_regOffline',
        name: 'salesRegOffline',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/SalesRegisterOffline.vue')
    },
    {
        path: '/dashboard/salesReturn_reg',
        name: 'salesReturnReg',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/SalesReturnRegister.vue')
    },
    {
        path: '/dashboard/deliveryNote_reg',
        name: 'deliveryNoteReg',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/DeliveryNoteRegister.vue')
    },
    {
        path: '/dashboard/quotation_reg',
        name: 'quotationReg',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/QuotationRegister.vue')
    },
    {
        path: '/dashboard/purchase_reg',
        name: 'purchaseReg',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/PurchaseRegister.vue')
    },
    {
        path: '/dashboard/purchaseReturn_reg',
        name: 'purchaseReturnReturnReg',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/PurchaseReturnRegister.vue')
    },
    {
        path: '/dashboard/journal_reg',
        name: 'journalReg',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/JournalRegister.vue')
    },
    {
        path: '/dashboard/goodsReceipt_reg',
        name: 'goodsReceiptReg',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/GoodsReceiptRegister.vue')
    },
    {
        path: '/dashboard/registers/ledger_reg',
        name: 'ledger_reg',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/LedgerRegister.vue')
    },
    {
        path: '/dashboard/stockTransfer_reg',
        name: 'stockTransferReg',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/StockTransferRegister.vue')
    },
    {
        path: '/dashboard/physicalStock_reg',
        name: 'physicalStockReg',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/PhysicalStockRegister.vue')
    },
    {
        path: '/dashboard/item_reg',
        name: 'itemReg',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/ItemRegister.vue')
    },
    {
        path: '/dashboard/registers/debitNote_reg',
        name: 'debitNoteReg',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/DebitNoteRegister.vue')
    },
    {
        path: '/dashboard/registers/creditNote_reg',
        name: 'creditNoteReg',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/CreditNoteRegister.vue')
    },
    {
        path: '/dashboard/dayBook',
        name: 'dayBook',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/DayBook.vue')
    },
    {
        path: '/dashboard/billsRecevable',
        name: 'billsRecevable',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Reports/BillsReceivable.vue')
    },
    {
        path: '/dashboard/receiptBillwaseAdjustment',
        name: 'receiptBillwaseAdjustment',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Others/ReceiptBillwaseAdjustment.vue')
    },
    {
        path: '/dashboard/paymentBillwaseAdjustment',
        name: 'paymentBillwaseAdjustment',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Others/PaymentBillwaseAdjustment.vue')
    },
    {
        path: '/dashboard/Sales_ReceiptAdjustments',
        name: 'sales_ReceiptAdjustments',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Others/SalesBillwaseAdjustment.vue')
    },
    {
        path: '/dashboard/journal_ReceiptAdjustments',
        name: 'journal_ReceiptAdjustments',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Others/JournalBillwaseAdjustment.vue')
    },
    {
        path: '/dashboard/DebitCredit_ReceiptAdjustments',
        name: 'DebitCredit_ReceiptAdjustments',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Others/DebitCreditNoteBillwaseAdjustment.vue')
    },
    {
        path: '/dashboard/masters/measureUnit',
        name: 'measureUnit',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/MeasureUnit.vue')
    },
    {
        path: '/dashboard/masters/ledger',
        name: 'ledger',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/Ledger.vue')
    },
     {
        path: '/dashboard/masters/LedgerGroup',
        name: 'LedgerGroup',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/LeadgerGroup.vue')
    },
    {
        path: '/dashboard/masters/godown',
        name: 'godown',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/Godown.vue')
    },
    {
        path: '/dashboard/masters/iframepdf',
        name: 'iframepdf',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/iframepdf.vue')
    },
    {
        path: '/dashboard/masters/customerProfile',
        name: 'customerProfile',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/CustomerProfile.vue')
  },
     {
        path: '/dashboard/masters/SetTenderingLedgers',
        name: 'SetTenderingLedgers',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/SetTenderingLedgers.vue')
    },
    {
        path: '/dashboard/registers/customerRegister',
        name: 'customerRegister',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/CustomerRegister.vue')
    },
    {
        path: '/dashboard/masters/supplierProfile',
        name: 'supplierProfile',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/SupplierProfile.vue')
    },
     {
        path: '/dashboard/masters/ClosingCashStatement',
        name: 'ClosingCashStatement',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/ClosingCashStatement.vue')
  },
      {
        path: '/dashboard/masters/Bank',
        name: 'Bank',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/Bank.vue')
    },
      {
        path: '/dashboard/masters/CostCenters',
        name: 'CostCenters',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/CostCenters.vue')
    },
    {
        path: '/dashboard/registers/supplierRegister',
        name: 'supplierRegister',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/SupplierRegister.vue')
    },
    {
        path: '/dashboard/registers/LedgerStatement',
        name: 'ledgerStatement',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/LedgerStatement.vue')
    },
    {
        path: '/dashboard/registers/ProjectRegister',
        name: 'ProjectRegister',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/ProjectRegister.vue')
    },
    {
        path: '/dashboard/registers/MaterialIssueRegister',
        name: 'MaterialIssueRegister',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/MaterialIssueRegister.vue')
    },
    {
        path: '/dashboard/registers/LeadRegister',
        name: 'LeadRegister',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/LeadRegister.vue')
    },
    {
        path: '/dashboard/registers/PurchaseOrder_reg',
        name: 'PurchaseOrder_reg',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/PurchaseOrderLegister.vue')
    },
     {
        path: '/dashboard/registers/ServiceBookingRegister',
        name: 'ServiceBookingRegister',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/ServiceBookingRegister.vue')
  },

    /* ***************************** Register Ends Here  ************************************** */








    /* ***************************** Masters Starts Here  ************************************** */
    {
        path: '/dashboard/masters/userProfile',
        name: 'userProfile',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/UserProfile.vue')
    },
    {
        path: '/dashboard/masters/assets',
        name: 'assets',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/Assets.vue')
    },
    {
        path: '/dashboard/masters/GodownStock',
        name: 'GodownStock',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/GodownStock.vue')
    },
    {
        path: '/dashboard/masters/ItemSuppliers',
        name: 'ItemSuppliers',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/ItemSuppliers.vue')
    },

    {
        path: '/dashboard/masters/update',
        name: 'update',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/Update.vue')
    },
    {
        path: '/dashboard/masters/bulkDamageTransfer',
        name: 'bulkDamageTransfer',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/BulkDamageTransfer.vue')
    },
    {
        path: '/dashboard/masters/BulkStockTransfer',
        name: 'BulkStockTransfer',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/BulkStockTransfer.vue')
    },
    {
        path: '/dashboard/masters/dayend',
        name: 'dayend',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/DayEnd.vue')
    },
    {
        path: '/dashboard/masters/itemMaster',
        name: 'itemMaster',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/Item.vue')
    },
    {
        path: '/dashboard/masters/userManagement',
        name: 'userManagement',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/UserManagement.vue')
    },
    {
        path: '/dashboard/masters/salesperson',
        name: 'salesperson',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/Salesperson.vue')
    },
    {
        path: '/dashboard/masters/purchaseperson',
        name: 'purchaseperson',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/Purchaseperson.vue')
    },
    {
        path: '/dashboard/masters/category',
        name: 'category',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/Category.vue')
    },
    {
        path: '/dashboard/masters/vehicle',
        name: 'vehicle',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/Vehicle.vue')
    },
    {
        path: '/dashboard/masters/AccessRoll',
        name: 'AccessRoll',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/AccessRoll.vue')
    },
    {
        path: '/dashboard/masters/CustomerRouteSector',
        name: 'CustomerRouteSector',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/CustomerRouteSector.vue')
    },
    {
        path: '/dashboard/masters/CustomertoRoute',
        name: 'CustomertoRoute',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/CustomertoRoute.vue')
    },
    {
        path: '/dashboard/masters/Project',
        name: 'Project',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/Project.vue')
    },
    {
        path: '/dashboard/masters/Contact',
        name: 'Contact',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/LeadContact.vue')
    },
    {
        path: '/dashboard/masters/LeadStatus',
        name: 'LeadStatus',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/LeadStatus.vue')
    },
      {
        path: '/dashboard/masters/Currency',
        name: 'Currency',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/Currency.vue')
  },
   {
        path: '/dashboard/masters/DefragmentDatabase',
        name: 'DefragmentDatabase',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/DefragmentDatabase.vue')
  },
  {
    path: '/dashboard/masters/VoiceTesting',
    name: 'VoiceTesting',
    component: () =>
        import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/VoiceTesting.vue')
},
    {
        path: '/dashboard/services/ServiceBooking',
        name: 'ServiceBooking',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Services/ServiceBooking.vue')
  },
  {
    path: '/dashboard/services/ServiceDashboard',
    name: 'ServiceDashboard',
    component: () =>
        import ( /* webpackChunkName: "dashboard" */ '@/pages/Services/ServiceDashboard.vue')
},
  {
    path: '/dashboard/services/ServiceTask',
    name: 'ServiceTask',
    component: () =>
        import ( /* webpackChunkName: "dashboard" */ '@/pages/Services/ServiceTask.vue')
},
    {
        path: '/dashboard/Registers/ServiceTaskRegister',
        name: 'ServiceTaskRegister',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Registers/ServiceTaskRegister.vue')
  },
  {
    path: '/dashboard/services/ServiceAssignment',
    name: 'ServiceTask',
    component: () =>
        import ( /* webpackChunkName: "dashboard" */ '@/pages/Services/ServiceAssignment.vue')
},

  {
        path: '/dashboard/services/ServiceHistory',
        name: 'ServiceHistory',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Services/ServiceHistory.vue')
  },
  

   {
        path: '/dashboard/masters/GlobalPassword',
        name: 'GlobalPassword',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/GlobalPassword.vue')
  },
     {
        path: '/dashboard/settings/VoucherSettings',
        name: 'VoucherSettings',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Settings/VoucherSettings.vue')
    },
    {
        path: '/dashboard/settings/RoleSettings',
        name: 'RoleSettings',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Settings/RoleSettings.vue')
  },
      {
        path: '/dashboard/settings/LockorUnlockVouchers',
        name: 'LockorUnlockVouchers',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Settings/LockorUnlockVouchers.vue')
  },
     {
        path: '/dashboard/settings/CustomerSignedSales',
        name: 'CustomerSignedSales',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Settings/CustomerSignedSales.vue')
  },
     {
        path: '/dashboard/masters/POSVoucherSettings',
        name: 'POSVoucherSettings',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Masters/POSVoucherSettings.vue')
    },
    {
        path: '/dashboard/settings/Company',
        name: 'Company',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Settings/Company.vue')
  },
    {
        path: '/dashboard/Settings/Branch',
        name: 'Branch',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Settings/Branch.vue')
  },
     {
        path: '/dashboard/settings/SalesPromotion',
        name: 'SalesPromotion',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Settings/SalesPromotion.vue')
  },
    {
        path: '/dashboard/settings/PointsandRewards',
        name: 'PointsandRewards',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Settings/PointsandRewards.vue')
  },
   {
        path: '/dashboard/settings/ItemDiscount',
        name: 'ItemDiscount',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Settings/ItemDiscount.vue')
  },
  {
         path: '/dashboard/settings/DiscountVoucher',
         name: 'DiscountVoucher',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Settings/DiscountVoucher.vue')
   },
    {
         path: '/dashboard/settings/DiscountCardList',
         name: 'DiscountCardList',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Settings/DiscountCardList.vue')
   },
    {
         path: '/dashboard/settings/DiscountCard',
         name: 'DiscountCard',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Settings/DiscountCard.vue')
   },
 {
         path: '/dashboard/settings/BundleOffer',
         name: 'BundleOffer',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Settings/BundleOffer.vue')
   },
    {
         path: '/dashboard/settings/CategoryFOC',
         name: 'CategoryFOC',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/Settings/CategoryFOC.vue')
   },
    /* ***************************** Masters End Here  ************************************** */



    /* ***************************** rough index  ************************************** */

    {
        path: '/dashboard/billwise',
        name: 'billwise123',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/dashboard/billwise.vue')
    },
    {
        path: '/dashboard/map',
        name: 'mapdistance',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/dashboard/maps/maps.vue')
    },
    // {
    //   path: '/dashboard/Realtime',
    //   name: 'Realtime',
    //   component: () => import(/* webpackChunkName: "dashboard" */ '@/pages/dashboard/maps/Realtime.vue')
    // },
    {
        path: '/dashboard/Realtime',
        name: 'realtime',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/dashboard/maps/Livelocation.vue')
    },
  {
        path: '/dashboard/RouteTimeLine',
        name: 'RouteTimeLine',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/dashboard/maps/RouteTimeLine.vue')
    },
    {
        path: '/dashboard/RouteMap',
        name: 'RouteMap',
        component: () =>
            import ( /* webpackChunkName: "dashboard" */ '@/pages/dashboard/maps/Routemap.vue')
    },
    /* ***************************** rough index  ************************************** */


    ...AppsRoutes,
    ...UIRoutes,
    ...PagesRoutes,
    ...UsersRoutes,
    ...EcommerceRoutes,
    ...LandingRoutes,
    {
        path: '/blank',
        name: 'blank',
        component: () =>
            import ( /* webpackChunkName: "blank" */ '@/pages/BlankPage.vue')
    },
    // {
    //   path: '*',
    //   name: 'error',
    //   component: () => import(/* webpackChunkName: "error" */ '@/pages/error/NotFoundPage.vue'),
    //   meta: {
    //     layout: 'error'
    //   }
    // }

]

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL || '/',
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) return savedPosition

        return { x: 0, y: 0 }
    },
    routes
})

/**
 * Before each route update
 */
router.beforeEach((to, from, next) => {
    return next()
})

/**
 * After each route update
 */
router.afterEach((to, from) => {})
export default router

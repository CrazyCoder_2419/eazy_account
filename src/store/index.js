import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { createStore } from 'vuex'
import VuexPersistence from 'vuex-persist'
// Global vuex
import AppModule from './app'


const headers = { Accept: "application/json" };
// localStorage.setItem("bURL"
const vuexLocal = new VuexPersistence({
    storage: window.localStorage
})

Vue.use(Vuex)

/**
 * Main Vuex Store
 */
const url_mob = 'Item/GetItemSummary?category=all&subcategory=all';
// const url_mob = 'Item';
const store = new Vuex.Store({

    modules: {
        app: AppModule,

    },
    state: {
        Items: [],
        cart: [],
        shopname: ''
    },
    mutations: {
        SET_ItemS(state, Items) {
            // state.Items = payload;
            state.Items = []
            state.Items.push(Items);
        },

        ADD_TO_CART(state, { Item, quantity, msg_ }) {
            // console.log("k " + JSON.stringify(Item) + " " + quantity)
            let ItemInthecart = state.cart.find(item => {
                return item.Item.ItemId === Item.ItemId
            })

            // console.log("bool " + ItemInthecart + " " + Item.ItemId)

            if (ItemInthecart) {
                ItemInthecart.quantity++
                    // let x = ItemInthecart.quantity;
                    // let y = quantity;
                    // let z = x + y;
                    // ItemInthecart.quantity = z
                    // return;
            } else {
                state.cart.push({
                    Item,
                    quantity,
                    msg_
                })
            }

        },
        ADD_QTY(state, { Item }) {
            let quantity = 1

            let ItemInthecart = state.cart.find(item => {
                return item.Item.ItemId === Item.ItemId
            })
            if (ItemInthecart) {
                ItemInthecart.quantity++
                    // return;
            }


        },
        MINUS_QTY(state, { Item }) {
            let quantity = 1

            let ItemInthecart = state.cart.find(item => {
                return item.Item.ItemId === Item.ItemId
            })
            if (ItemInthecart) {
                if (ItemInthecart.quantity != 1) {
                    ItemInthecart.quantity--
                }

                // return;
            }
        },

        REMOVE_FROM_CART(state, { Item }) {
            // console.log(Item.prdct_id)
            state.cart = state.cart.filter(item => {
                return item.Item.ItemId !== Item.ItemId
            })
        },
        CLEAR_CART(state) {
            state.cart = []
        },

    },
    actions: {
        async getItems(state) {
            //   this.burl = localStorage.getItem('bURL');
            this.bURL = localStorage.getItem("CompanyURL") + "/api/";
            const result = await fetch(this.bURL + url_mob);
            const Item = await result.json();
            state.commit("SET_ItemS", Item);
        },

        async addTocart(state, { Item, quantity, msg_ }) {
            // console.log("Test " + JSON.stringify(Item) + " " + quantity)
            state.commit("ADD_TO_CART", { Item, quantity, msg_ });
        },

        async addqty(state, { Item }) {
            // console.log(Item)
            state.commit("ADD_QTY", { Item });
        },

        async minusqty(state, { Item }) {
            state.commit("MINUS_QTY", { Item });
        },
        async removeCart(state, { Item, Item_id }) {
            // console.log(Item.name)
            state.commit("REMOVE_FROM_CART", { Item });
        },
        async clearCartItems(state) {
            state.commit("CLEAR_CART");
        }
    },
    getters: {
        totalprice: state => {
            let Total = 0;
            state.cart.forEach(item => {
                Total += item.Item.RetailRate * item.quantity;
            })

            return Total;
        },


        // getAllJokes: state => state.allJokes
    },
    plugins: [vuexLocal.plugin]

})

export default store